/* eslint-disable no-undef */
/* eslint-disable no-alert */
import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Grid,
    TextField,
    Checkbox,
    FormControlLabel,
    MenuItem,
    Button,
    Select,
} from '@material-ui/core';
// import { mask, unMask } from 'remask';
import { useForm } from 'react-hook-form';

import { toast, ToastContainer } from 'react-toastify';
import api from '../../services/api';

const useStyles = makeStyles(() => ({
    root: {
        '& .MuiTextField-root': {
            margin: 'auto',
            width: '100%',
        },
    },

    container: {
        border: '1px solid #c1c1c1',
        borderRadius: '7px',
        margin: 'auto',
        marginTop: 10,
        padding: 30,
    },
    row: {
        display: 'flex',
        flexDirection: 'rows',
        justifyContent: 'space-between',
        margin: '2rem',
    },
    span: {
        display: 'flex',
        color: '#FF0000',
        fontSize: 11,
        marginLeft: 8,
        marginTop: -5,
    },
}));

export default function FormFamilies(props) {
    const classes = useStyles();

    const salario = 1100.0;
    const [isEnabled, setIsEnabled] = useState(true);
    const [familia, setFamilia] = useState([]);
    const [renda, setRenda] = useState(0);
    const [reside, setReside] = useState('');
    const [prontuario, setProntuario] = useState('');
    const [nota, setNota] = useState('');

    const { handleSubmit, errors, control, register } = useForm();
    const user = JSON.parse(sessionStorage.getItem('USERAGENT'));

    useEffect(async () => {
        await api
            .get(`/familiashow?id=${user.id}&prontuario=${props.family}`)
            .then((response) => {
                setFamilia(response.data.familia[0].nome_logradouro);
                setRenda(response.data.familia[0].rendafamiliar);
                setReside(response.data.familia[0].reside_desde);
                setProntuario(response.data.familia[0].prontuario_familiar);
                setNota(response.data.familia[0].anotacao);
            });
    }, []);

    const notifyError = (error) => {
        toast.error(
            `MMS - Error Notification -Operação não foi concluída!\n ${error}`,
            {
                position: toast.POSITION.TOP_RIGH,
            }
        );
    };

    const notifySuccess = () => {
        toast.success(
            'MMS - Success Notification - Operação efetuada com sucesso!',
            {
                position: toast.POSITION.TOP_RIGH,
            }
        );
    };
    const onSubmit = (data, e) => {
        try {
            api.put('/familiacad', { user, data });
            notifySuccess();
            e.target.reset();
        } catch (error) {
            notifyError(error);
        }
    };

    return (
        <form
            className={classes.root}
            onSubmit={handleSubmit(onSubmit)}
            noValidate
            autoComplete="off"
        >
            <div className={classes.container}>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <TextField
                            label="Domicilio da Família"
                            fullWidth
                            type="text"
                            value={`${familia}`}
                            inputRef={register}
                            disabled
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            name="prontuario_familiar"
                            label="Nº do Prontuário ou Cod. Famíliar"
                            type="number"
                            inputRef={register({ required: true })}
                            value={prontuario}
                            fullWidth
                            error={!!errors.prontuario_familiar}
                            required
                            disabled={isEnabled}
                        />
                        {errors.prontuario_familiar &&
                            errors.prontuario_familiar.type === 'required' && (
                                <span className={classes.span}>
                                    Este campo é obrigatório!
                                </span>
                            )}
                    </Grid>
                    <Grid item md sm xs>
                        <Select
                            id="rendafamiliar"
                            name="rendafamiliar"
                            label="Renda Familiar"
                            control={control}
                            value={renda}
                            onChange={(e) => setRenda(e.target.value)}
                            fullWidth
                            inputProps={{
                                inputRef: (ref) => {
                                    if (!ref) return;
                                    register({
                                        name: 'rendafamiliar',
                                        value: ref.value,
                                    });
                                },
                            }}
                            error={!!errors.rendafamiliar}
                            required
                            disabled={isEnabled}
                        >
                            <MenuItem value={0} />
                            <MenuItem value={1}>
                                1/4 salário mínimo (R${' '}
                                {Number(salario / 4).toFixed(2)})
                            </MenuItem>
                            <MenuItem value={2}>
                                1/2 salário mínimo (R${' '}
                                {Number(salario / 2).toFixed(2)})
                            </MenuItem>
                            <MenuItem value={3}>
                                1 salário mínimo (R${' '}
                                {Number(salario).toFixed(2)} )
                            </MenuItem>
                            <MenuItem value={4}>
                                2 salários mínimos (R${' '}
                                {Number(salario * 2).toFixed(2)})
                            </MenuItem>
                            <MenuItem value={5}>
                                3 salários mínimos (R${' '}
                                {Number(salario * 3).toFixed(2)})
                            </MenuItem>
                            <MenuItem value={6}>
                                4 salários mínimos (R${' '}
                                {Number(salario * 4).toFixed(2)})
                            </MenuItem>
                            <MenuItem value={7}>
                                + de 4 salários mínimos
                            </MenuItem>
                            <MenuItem value={8}>Não Informada</MenuItem>
                        </Select>
                        {errors.rendafamiliar &&
                            errors.rendafamiliar.type === 'required' && (
                                <span className={classes.span}>
                                    Este campo é obrigatório!
                                </span>
                            )}
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <TextField
                            name="reside_desde"
                            label="Reside Desde (ex.: 01/2015)"
                            fullWidth
                            type="text"
                            value={reside}
                            inputRef={register}
                            disabled={isEnabled}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    name="familia_mudou"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Mudança de Domicílio"
                            variant="filled"
                            disabled={isEnabled}
                        />
                    </Grid>
                    <Grid item md sm xs />
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <TextField
                            id="anotacao"
                            name="anotacao"
                            label="Anotações"
                            value={nota}
                            onChange={(e) => setNota(e.target.value)}
                            multiline
                            onKeyUp
                            rows={4}
                            variant="outlined"
                            inputRef={register}
                            disabled={isEnabled}
                        />
                    </Grid>
                </Grid>
            </div>
            <div className={classes.row}>
                {!isEnabled ? (
                    <Button variant="contained" color="primary" type="submit">
                        Atualizar Familia
                    </Button>
                ) : (
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={() => setIsEnabled(!isEnabled)}
                    >
                        Editar Familia
                    </Button>
                )}

                <Button
                    variant="contained"
                    color="default"
                    onClick={props.close}
                >
                    Cancelar
                </Button>
            </div>
            <ToastContainer />
        </form>
    );
}
