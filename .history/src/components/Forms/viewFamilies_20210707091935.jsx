/* eslint-disable no-undef */
/* eslint-disable no-alert */
import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Grid,
    TextField,
    Checkbox,
    FormControlLabel,
    MenuItem,
    Button,
} from '@material-ui/core';
import { mask, unMask } from 'remask';
import { useForm } from 'react-hook-form';

import { toast, ToastContainer } from 'react-toastify';
import FormSelect from '../controlls/select/index';
import api from '../../services/api';

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '100%',
        },
    },
    container: {
        border: '1px solid #c1c1c1',
        borderRadius: '7px',
        margin: 'auto',
        marginTop: 10,
        padding: 30,
    },
    row: {
        display: 'flex',
        flexDirection: 'rows',
        justifyContent: 'space-between',
        margin: '2rem',
    },
    span: {
        display: 'flex',
        color: '#FF0000',
        fontSize: 11,
        marginLeft: 8,
        marginTop: -5,
    },
}));

export default function FormFamilies(props) {
    const classes = useStyles();

    const salario = 1100.0;
    const [changeAdressEnabled] = useState(true);
    const [date, setDate] = useState('');
    const [renda, setRenda] = useState(0);
    const [domicilio, setDomicilio] = useState([]);
    const [changeAdress] = useState({
        checked: false,
    });

    const { handleSubmit, errors, control, register } = useForm();
    const user = JSON.parse(sessionStorage.getItem('USERAGENT'));

    useEffect(async () => {
        await api
            .get(`/familiashow?id=${user.id}&prontuario=${props.family}`)
            .then((response) => {
                setFamilia(response.data.familia[0]);
                setRenda(response.data.familia[0].rendafamiliar);
                setDomicilio(response.data.familia[0].nome_logradouro);
            });
    }, []);

    const notifyError = (error) => {
        toast.error(
            `MMS - Error Notification -Operação não foi concluída!\n ${error}`,
            {
                position: toast.POSITION.TOP_RIGH,
            }
        );
    };

    const notifySuccess = () => {
        toast.success(
            'MMS - Success Notification - Operação efetuada com sucesso!',
            {
                position: toast.POSITION.TOP_RIGH,
            }
        );
    };
    const onSubmit = (data, e) => {
        try {
            api.post('/familiacad', { user, data });
            notifySuccess();
            e.target.reset();
        } catch (error) {
            notifyError(error);
        }
    };

    const handleDate = (event) => {
        setDate(mask(unMask(event.target.value), ['99/9999']));
    };

    return (
        <form
            className={classes.root}
            onSubmit={handleSubmit(onSubmit)}
            noValidate
            autoComplete="off"
        >
            <div className={classes.container}>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <TextField
                            name="Domicilio da Família"
                            label="Domicilio da Família"
                            type="text"
                            fullWidth
                            disabled
                            value={domicilio}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            name="prontuario_familiar"
                            label="Nº do Prontuário ou Cod. Famíliar"
                            type="number"
                            inputRef={register({ required: true })}
                            fullWidth
                            error={!!errors.prontuario_familiar}
                            required
                        />
                        {errors.prontuario_familiar &&
                            errors.prontuario_familiar.type === 'required' && (
                                <span className={classes.span}>
                                    Este campo é obrigatório!
                                </span>
                            )}
                    </Grid>
                    <Grid item md sm xs>
                        <FormSelect
                            id="rendafamiliar"
                            name="rendafamiliar"
                            label="Renda Familiar"
                            control={control}
                            value={renda}
                            onChange={(e) => setRenda(e.target.value)}
                            fullWidth
                            inputRef={register({ required: true })}
                            error={!!errors.rendafamiliar}
                            required
                        >
                            <MenuItem value={0} />
                            <MenuItem value={1}>
                                1/4 salário mínimo (R${' '}
                                {Number(salario / 4).toFixed(2)})
                            </MenuItem>
                            <MenuItem value={2}>
                                1/2 salário mínimo (R${' '}
                                {Number(salario / 2).toFixed(2)})
                            </MenuItem>
                            <MenuItem value={3}>
                                1 salário mínimo (R${' '}
                                {Number(salario).toFixed(2)} )
                            </MenuItem>
                            <MenuItem value={4}>
                                2 salários mínimos (R${' '}
                                {Number(salario * 2).toFixed(2)})
                            </MenuItem>
                            <MenuItem value={5}>
                                3 salários mínimos (R${' '}
                                {Number(salario * 3).toFixed(2)})
                            </MenuItem>
                            <MenuItem value={6}>
                                4 salários mínimos (R${' '}
                                {Number(salario * 4).toFixed(2)})
                            </MenuItem>
                            <MenuItem value={7}>
                                + de 4 salários mínimos
                            </MenuItem>
                            <MenuItem value={8}>Não Informada</MenuItem>
                        </FormSelect>
                        {errors.rendafamiliar &&
                            errors.rendafamiliar.type === 'required' && (
                                <span className={classes.span}>
                                    Este campo é obrigatório!
                                </span>
                            )}
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <TextField
                            name="reside_desde"
                            label="Reside Desde (ex.: 01/2015)"
                            fullWidth
                            type="text"
                            onChange={handleDate}
                            value={date}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    checked={changeAdress.checked}
                                    name="familia_mudou"
                                    color="primary"
                                />
                            }
                            label="Mudança de Domicílio"
                            variant="filled"
                            disabled={changeAdressEnabled}
                        />
                    </Grid>
                    <Grid item md sm xs />
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <TextField
                            id="anotacao"
                            name="anotacao"
                            label="Anotações"
                            multiline
                            onKeyUp
                            rows={4}
                            variant="outlined"
                            inputRef={register}
                        />
                    </Grid>
                </Grid>
            </div>
            <div className={classes.row}>
                <Button variant="contained" color="primary" type="submit">
                    Cadastrar
                </Button>

                <Button
                    variant="contained"
                    color="default"
                    onClick={props.close}
                >
                    Cancelar
                </Button>
            </div>
            <ToastContainer />
        </form>
    );
}
