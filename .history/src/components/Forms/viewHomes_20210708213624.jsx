/* eslint-disable no-useless-escape */
/* eslint-disable no-shadow */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-undef */
/* eslint-disable no-alert */
import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Grid,
    Typography,
    MenuItem,
    TextField,
    Checkbox,
    FormControlLabel,
    Button,
} from '@material-ui/core';
import InputMask from 'react-input-mask';
import { mask, unMask } from 'remask';
import { useForm, Controller } from 'react-hook-form';
import { Autocomplete } from '@material-ui/lab';
import { toast, ToastContainer } from 'react-toastify';
import Ibgeapi from '../../services/ibgeApi';
import FormSelect from '../controlls/select/index';
import api from '../../services/api';

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '100%',
        },
    },
    container: {
        border: '1px solid #c1c1c1',
        borderRadius: '7px',
        margin: 'auto',
        marginTop: 10,
        padding: 30,
    },
    row: {
        display: 'flex',
        flexDirection: 'rows',
        justifyContent: 'space-between',
        margin: '2rem',
    },
    span: {
        display: 'flex',
        color: '#FF0000',
        fontSize: 11,
        marginLeft: 8,
        marginTop: -5,
    },
}));

export default function FormHomes(props) {
    const classes = useStyles();
    const { register, handleSubmit, control, errors } = useForm();
    const [disableMicroArea, setDisableMicroArea] = useState(false);
    const [disableNumber, setDisableNumber] = useState(false);
    const [enableAnimals, setEnableAnimals] = useState(false);
    const [isEnabled] = useState(false);
    const [allEnabled, setAllEnabled] = useState(true);

    const [immobile, setImmobile] = useState('');
    const [location, setLocation] = useState('');
    const [cities, setCities] = useState([]);
    const [tipoLogradouro, setTipoLogradouro] = useState([]);
    const [logradouro, setLogradouro] = useState([]);
    const [phone, setPhone] = useState('');
    const [homethePhone, setHomePhone] = useState('');
    const [contactPhone, setContactPhone] = useState('');
    const { id } = JSON.parse(sessionStorage.getItem('USERAGENT'));

    useEffect(() => {
        Ibgeapi.get('municipios')
            .then((response) => {
                setCities(response.data);
            })
            .catch(Error);
    }, []);

    useEffect(async () => {
        await api
            .get('/tipologradouro')
            .then((response) => {
                setTipoLogradouro(response.data.tipoLogradouro);
            })
            .catch(Error);
    }, []);
    useEffect(async () => {
        await api
            .get(`/domicilios?id=${id}`)
            .then((response) => {
                setLogradouro(response.data.domicilios);
            })
            .catch(Error);
    }, []);

    const user = JSON.parse(sessionStorage.getItem('USERAGENT'));

    const notifyError = (error) => {
        toast.error(
            `MMS - Error Notification -Operação não foi concluída!${error}`,
            {
                position: toast.POSITION.TOP_RIGH,
            }
        );
    };

    const notifySuccess = (success) => {
        toast.success(
            `MMS - Success Notification - Operação efetuada com sucesso! uuid_ficha_originadora: ${success}`,
            {
                position: toast.POSITION.TOP_RIGH,
            }
        );
    };

    const onSubmit = async (data, e) => {
        try {
            const response = await api.post('/domiciliocad', { data, user });
            notifySuccess(response.data.uuid_ficha_originadora);
            e.target.reset();
        } catch (error) {
            notifyError(error);
        }
    };

    const onchangePhone = (ev) => {
        setPhone(
            mask(unMask(ev.target.value), [
                '(99) 9999-9999',
                '(99) 9 9999-9999',
            ])
        );
    };
    const onchangeHomePhone = (ev) => {
        setHomePhone(
            mask(unMask(ev.target.value), [
                '(99) 9999-9999',
                '(99) 9 9999-9999',
            ])
        );
    };
    const onchangeContactPhone = (ev) => {
        setContactPhone(
            mask(unMask(ev.target.value), [
                '(99) 9999-9999',
                '(99) 9 9999-9999',
            ])
        );
    };

    const handleChecked = (event, isChecked) => {
        setDisableMicroArea(isChecked);
    };

    const handleCheckedAnimals = (event, isChecked) => {
        setEnableAnimals(isChecked);
    };
    const handleCheckedNumber = (event, isChecked) => {
        setDisableNumber(isChecked);
        setAllEnabled(false);
    };

    return (
        <form
            className={classes.root}
            onSubmit={handleSubmit(onSubmit)}
            noValidate
            autoComplete="off"
        >
            <div className={classes.container}>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <TextField
                            disabled={allEnabled}
                            name="microarea"
                            label="Micro-área"
                            inputProps={{ maxLength: 2 }}
                            inputRef={register({ required: !disableMicroArea })}
                            fullWidth
                            required
                            error={!!errors.microarea}
                        />
                        {errors.microarea &&
                            errors.microarea.type === 'required' && (
                                <span className={classes.span}>
                                    Este campo é obrigatório!
                                </span>
                            )}
                    </Grid>
                    <Grid item className={classes.text}>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    inputRef={register}
                                    defaultValue={disableMicroArea}
                                    onChange={handleChecked}
                                    color="primary"
                                />
                            }
                            name="st_fora_area"
                            label="Fora da área"
                            variant="filled"
                            disabled={allEnabled}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormSelect
                            id="tipo_imovel"
                            name="tipo_imovel"
                            label="Tipo de Imóvel"
                            control={control}
                            disabled={allEnabled}
                            defaultValue=""
                            fullWidth
                            onClick={(e) => setImmobile(e.target.value)}
                            required
                            inputRef={register({ required: true })}
                            error={!!errors.tipo_imovel}
                        >
                            <MenuItem value={0} />
                            <MenuItem value={1}>Domicílio</MenuItem>
                            <MenuItem value={2}>Comércio</MenuItem>
                            <MenuItem value={3}>Terreno Baldio</MenuItem>
                            <MenuItem value={4}>
                                Ponto Estratégico (cemitério, borracharia,
                                ferro-velho, ...)
                            </MenuItem>
                            <MenuItem value={5}>Escola</MenuItem>
                            <MenuItem value={6}>Creche</MenuItem>
                            <MenuItem value={7}>Abrigo</MenuItem>
                            <MenuItem value={8}>
                                Instituição de longa permanência para idosos
                            </MenuItem>
                            <MenuItem value={9}>Unidade Prisional</MenuItem>
                            <MenuItem value={10}>
                                Unidade de medida sócio educativa
                            </MenuItem>
                            <MenuItem value={11}>Delegacia</MenuItem>
                            <MenuItem value={12}>
                                Estabelecimento religioso
                            </MenuItem>
                            <MenuItem value={13}>Outros</MenuItem>
                        </FormSelect>
                        {errors.tipo_imovel &&
                            errors.tipo_imovel.type === 'required' && (
                                <span className={classes.span}>
                                    Este campo é obrigatório!
                                </span>
                            )}
                    </Grid>
                    <Grid item md sm xs>
                        <Controller
                            render={({ onChange }) => (
                                <Autocomplete
                                    freeSolo
                                    options={tipoLogradouro}
                                    disabled={allEnabled}
                                    getOptionLabel={(option) => option.nome}
                                    renderOption={(option) => option.nome}
                                    getOptionSelected={(option) =>
                                        option.numero_dne
                                    }
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            label="Tipo de Logradouro"
                                            required
                                            error={
                                                !!errors.tipo_logradouro_numero_dne
                                            }
                                        />
                                    )}
                                    onChange={(e) => onChange(e.target.value)}
                                    {...props}
                                />
                            )}
                            onChange={([, data]) => data.numero_dne}
                            name="tipo_logradouro_numero_dne"
                            control={control}
                            rules={{ required: true }}
                            inputRef={register({
                                required: true,
                            })}
                            required
                            error={!!errors.tipo_logradouro_numero_dne}
                        />
                        {errors.tipo_logradouro_numero_dne &&
                            errors.tipo_logradouro_numero_dne.type ===
                                'required' && (
                                <span className={classes.span}>
                                    Este campo é obrigatório!
                                </span>
                            )}
                    </Grid>
                </Grid>
                {immobile === 7 ||
                immobile === 8 ||
                immobile === 9 ||
                immobile === 10 ||
                immobile === 11 ? (
                    <>
                        <Grid container spacing={4} alignItems="center">
                            <Grid item md sm xs>
                                <TextField
                                    name="nome_instituicao_permanencia"
                                    label="Nome da Instituição de Permanência"
                                    fullWidth
                                    disabled={allEnabled}
                                    inputRef={register}
                                />
                            </Grid>
                        </Grid>
                        <Grid container spacing={4} alignItems="center">
                            <Grid item md sm xs>
                                <Typography variant="h6">
                                    <strong>
                                        Dados do Responsável Técnico da
                                        Instituição:
                                    </strong>
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid container spacing={4} alignItems="center">
                            <Grid item md sm xs>
                                <TextField
                                    name="nome_responsavel_tecnico"
                                    label="Nome do Reponsável Técnico "
                                    fullWidth
                                    disabled={allEnabled}
                                    required
                                    inputRef={register({
                                        required: !isEnabled,
                                    })}
                                    error={!!errors.nome_responsavel_tecnico}
                                />
                                {errors.nome_responsavel_tecnico &&
                                    errors.nome_responsavel_tecnico.type ===
                                        'required' && (
                                        <span className={classes.span}>
                                            Este campo é obrigatório!
                                        </span>
                                    )}
                            </Grid>
                            <Grid item md sm xs>
                                <TextField
                                    name="cns_responsavel_tecnico"
                                    label="CNS do Responsável Técnico"
                                    type="number"
                                    disabled={allEnabled}
                                    inputRef={register}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item md sm xs>
                                <TextField
                                    name="cargo_responsavel_tecnico"
                                    label="Cargo do Responsável Técnico"
                                    inputRef={register}
                                    disabled={allEnabled}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item md sm xs>
                                <TextField
                                    name="telefone_responsavel_tecnico"
                                    label="Telefone do Responsável Técnico"
                                    onChange={onchangePhone}
                                    value={phone}
                                    inputRef={register}
                                    disabled={allEnabled}
                                    fullWidth
                                />
                            </Grid>
                        </Grid>
                        <Grid container spacing={4} alignItems="center">
                            <Grid item md sm xs>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            inputRef={register}
                                            color="primary"
                                        />
                                    }
                                    label="Existem outros profissionais de saúde vinculados à instituição (não inclui profissionais da rede pública de saúde)"
                                    variant="filled"
                                    disabled={allEnabled}
                                    name="outros_profissionais_vinculados"
                                />
                            </Grid>
                        </Grid>
                    </>
                ) : (
                    <></>
                )}
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <Autocomplete
                            freeSolo
                            disableClearable
                            disabled={allEnabled}
                            fullWidth
                            options={logradouro.map(
                                (option) => option.nome_logradouro
                            )}
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    label="Nome do Logradouro"
                                    name="nome_logradouro"
                                    margin="normal"
                                    inputRef={register({
                                        required: true,
                                    })}
                                    error={!!errors.nome_logradouro}
                                    required
                                    InputProps={{
                                        ...params.InputProps,
                                        type: 'search',
                                    }}
                                />
                            )}
                        />
                        {errors.nome_logradouro &&
                            errors.nome_logradouro.type === 'required' && (
                                <span className={classes.span}>
                                    Este campo é obrigatório!
                                </span>
                            )}
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            name="numero"
                            label="Número "
                            inputRef={register({ required: !disableNumber })}
                            fullWidth
                            disabled={allEnabled}
                            required
                            error={!!errors.numero}
                        />
                        {errors.numero && errors.numero.type === 'required' && (
                            <span className={classes.span}>
                                Este campo é obrigatório!
                            </span>
                        )}
                    </Grid>
                    <Grid item className={classes.text}>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    inputRef={register}
                                    checked={disableNumber}
                                    onChange={handleCheckedNumber}
                                    color="primary"
                                />
                            }
                            label="Sem Número"
                            variant="filled"
                            disabled={allEnabled}
                            name="st_sem_numero"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <Autocomplete
                            freeSolo
                            disableClearable
                            disabled={allEnabled}
                            fullWidth
                            options={logradouro.map((option) => option.bairro)}
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    label="Bairro"
                                    name="bairro"
                                    margin="normal"
                                    inputRef={register({
                                        required: true,
                                    })}
                                    error={!!errors.bairro}
                                    required
                                    InputProps={{
                                        ...params.InputProps,
                                        type: 'search',
                                    }}
                                />
                            )}
                        />
                        {errors.bairro && errors.bairro.type === 'required' && (
                            <span className={classes.span}>
                                Este campo é obrigatório!
                            </span>
                        )}
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <TextField
                            name="complemento"
                            label="Complemento"
                            inputRef={register}
                            fullWidth
                            disabled={allEnabled}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            name="ponto_referencia"
                            label="Ponto de Referência"
                            inputRef={register}
                            disabled={allEnabled}
                            fullWidth
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <Autocomplete
                            freeSolo
                            disableClearable
                            disabled={allEnabled}
                            fullWidth
                            options={logradouro.map((option) => option.cep)}
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    label="CEP"
                                    name="cep"
                                    margin="normal"
                                    inputRef={register({
                                        required: true,
                                    })}
                                    error={!!errors.cep}
                                    required
                                    InputProps={{
                                        ...params.InputProps,
                                        type: 'search',
                                    }}
                                >
                                    <InputMask mask="99.999-999" maskChar=" " />
                                </TextField>
                            )}
                        />
                        {errors.cep && errors.cep.type === 'required' && (
                            <span className={classes.span}>
                                Este campo é obrigatório!
                            </span>
                        )}
                    </Grid>
                    <Grid item md sm xs>
                        <Controller
                            render={({ onChange }) => (
                                <Autocomplete
                                    freeSolo
                                    disabled={allEnabled}
                                    options={cities}
                                    getOptionLabel={(option) =>
                                        `${option.nome} - ${option.microrregiao.mesorregiao.UF.sigla}`
                                    }
                                    renderOption={(option) =>
                                        `${option.nome} - ${option.microrregiao.mesorregiao.UF.sigla}`
                                    }
                                    getOptionSelected={(option) => option.id}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            label="Cidade - UF"
                                            required
                                            error={!!errors.cod_ibge_municipio}
                                        />
                                    )}
                                    onChange={(e) => onChange(e.target.value)}
                                    {...props}
                                />
                            )}
                            onChange={([, data]) => data.id}
                            name="cod_ibge_municipio"
                            control={control}
                            rules={{ required: true }}
                            inputRef={register({
                                required: true,
                            })}
                            required
                            error={!!errors.cod_ibge_municipio}
                        />
                        {errors.cod_ibge_municipio &&
                            errors.cod_ibge_municipio.type === 'required' && (
                                <span className={classes.span}>
                                    Este campo é obrigatório!
                                </span>
                            )}
                    </Grid>

                    <Grid item md sm xs>
                        <TextField
                            name="telelefone_residencia"
                            label="Telefone Residencial"
                            inputRef={register}
                            onChange={onchangeHomePhone}
                            value={homethePhone}
                            disabled={allEnabled}
                            fullWidth
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            name="telefone_contato"
                            label="Telefone para Contato"
                            inputRef={register}
                            onChange={onchangeContactPhone}
                            value={contactPhone}
                            disabled={allEnabled}
                            fullWidth
                        />
                    </Grid>
                </Grid>

                {immobile === 1 ? (
                    <>
                        <Grid container spacing={4} alignItems="center">
                            <Grid item md sm xs>
                                <Typography variant="h6">
                                    <strong>Condições de Moradia:</strong>
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid container spacing={4} alignItems="center">
                            <Grid item md sm xs>
                                <FormSelect
                                    id="situacao_moradia_posse_terra"
                                    name="situacao_moradia_posse_terra"
                                    label="Situação de moradia ou de posse da terra"
                                    control={control}
                                    defaultValue=""
                                    fullWidth
                                    disabled={allEnabled}
                                    required
                                    inputRef={register({ required: true })}
                                    error={
                                        !!errors.situacao_moradia_posse_terra
                                    }
                                >
                                    <MenuItem value="" />
                                    <MenuItem value={1}>Próprio</MenuItem>
                                    <MenuItem value={2}>Financiado</MenuItem>
                                    <MenuItem value={3}>Alugado</MenuItem>
                                    <MenuItem value={4}>Arrendado</MenuItem>
                                    <MenuItem value={5}>Cedido</MenuItem>
                                    <MenuItem value={6}>Ocupação</MenuItem>
                                    <MenuItem value={7}>
                                        Situação de rua
                                    </MenuItem>
                                    <MenuItem value={8}>Outra</MenuItem>
                                </FormSelect>
                                {errors.situacao_moradia_posse_terra &&
                                    errors.situacao_moradia_posse_terra.type ===
                                        'required' && (
                                        <span className={classes.span}>
                                            Este campo é obrigatório!
                                        </span>
                                    )}
                            </Grid>
                            <Grid item md sm xs>
                                <FormSelect
                                    id="localizacao"
                                    name="localizacao"
                                    label="Localização"
                                    control={control}
                                    defaultValue={location}
                                    onClick={(e) => setLocation(e.target.value)}
                                    helperText="Porfavor selecione uma opção"
                                    fullWidth
                                    disabled={allEnabled}
                                    required
                                    inputRef={register({ required: true })}
                                    error={!!errors.localizacao}
                                >
                                    <MenuItem />
                                    <MenuItem value={1}>Urbana</MenuItem>
                                    <MenuItem value={2}>Rural</MenuItem>
                                </FormSelect>
                                {errors.localizacao &&
                                    errors.localizacao.type === 'required' && (
                                        <span className={classes.span}>
                                            Este campo é obrigatório!
                                        </span>
                                    )}
                            </Grid>
                            <Grid item md sm xs>
                                <FormSelect
                                    id="condicao_posse_uso_terra"
                                    name="condicao_posse_uso_terra"
                                    label="Condição de Posse e Uso da Terra"
                                    control={control}
                                    inputRef={register}
                                    defaultValue=""
                                    disabled={location !== 2}
                                    fullWidth
                                >
                                    <MenuItem value="" />
                                    <MenuItem value={1}>Proprietário</MenuItem>
                                    <MenuItem value={2}>
                                        Parceiro Meeiro
                                    </MenuItem>
                                    <MenuItem value={3}>Assentado</MenuItem>
                                    <MenuItem value={4}>Posseiro</MenuItem>
                                    <MenuItem value={5}>Arrendatário</MenuItem>
                                    <MenuItem value={6}>Comodatário</MenuItem>
                                    <MenuItem value={7}>
                                        Beneficiário do Banco da Terra
                                    </MenuItem>
                                    <MenuItem value={8}>Não se Aplica</MenuItem>
                                </FormSelect>
                            </Grid>
                            <Grid item md sm xs>
                                <FormSelect
                                    id="tipo_domicilio"
                                    name="tipo_domicilio"
                                    label="Tipo do Domicílio"
                                    inputRef={register}
                                    control={control}
                                    defaultValue=""
                                    disabled={allEnabled}
                                    fullWidth
                                >
                                    <MenuItem value="" />
                                    <MenuItem value={1}>Casa</MenuItem>
                                    <MenuItem value={2}>Apartamento</MenuItem>
                                    <MenuItem value={3}>Cômodo</MenuItem>
                                    <MenuItem value={4}>Outro</MenuItem>
                                </FormSelect>
                            </Grid>
                        </Grid>
                        <Grid container spacing={4} alignItems="center">
                            <Grid item md sm xs>
                                <FormSelect
                                    id="tipo_acesso_domicilio"
                                    name="tipo_acesso_domicilio"
                                    label="Tipo de acesso"
                                    control={control}
                                    defaultValue=""
                                    helperText="Porfavor selecione uma opção"
                                    disabled={allEnabled}
                                    fullWidth
                                >
                                    <MenuItem value="" />
                                    <MenuItem value={1}>Pavimento</MenuItem>
                                    <MenuItem value={2}>Chão Batido</MenuItem>
                                    <MenuItem value={3}>Fluvial</MenuItem>
                                    <MenuItem value={4}>Outro</MenuItem>
                                </FormSelect>
                            </Grid>
                            <Grid item md sm xs>
                                <TextField
                                    name="qtd_moradores"
                                    label="Nº de Moradores"
                                    type="number"
                                    inputRef={register}
                                    disabled={allEnabled}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item md sm xs>
                                <TextField
                                    name="qtd_comodos"
                                    label="Nº de Cômodos"
                                    type="number"
                                    inputRef={register}
                                    disabled={allEnabled}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item md sm xs>
                                <FormSelect
                                    id="material_predominante_paredes_ext_domicilio"
                                    name="material_predominante_paredes_ext_domicilio"
                                    label="Material Predominante na Construção das Paredes"
                                    control={control}
                                    defaultValue=""
                                    helperText="Porfavor selecione uma opção"
                                    disabled={allEnabled}
                                    fullWidth
                                >
                                    <MenuItem value="" />
                                    <MenuItem value={1}>
                                        Alvenaria com Revestimento
                                    </MenuItem>
                                    <MenuItem value={2}>
                                        Alvenaria sem Revestimento
                                    </MenuItem>
                                    <MenuItem value={3}>
                                        Taipa com Revestimento
                                    </MenuItem>
                                    <MenuItem value={4}>
                                        Taipa sem Revestimento
                                    </MenuItem>
                                    <MenuItem value={5}>
                                        Madeira emparelhada
                                    </MenuItem>
                                    <MenuItem value={6}>
                                        Material aproveitado
                                    </MenuItem>
                                    <MenuItem value={7}>Palha</MenuItem>
                                    <MenuItem value={8}>
                                        Outro Material
                                    </MenuItem>
                                </FormSelect>
                            </Grid>
                        </Grid>
                        <Grid container spacing={4} alignItems="center">
                            <Grid item className={classes.text}>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            name="disponibilidade_energia_eletrica"
                                            inputRef={register}
                                            color="primary"
                                        />
                                    }
                                    label="Possui Energia Elétrica"
                                    variant="filled"
                                    disabled={allEnabled}
                                />
                            </Grid>
                            <Grid item className={classes.text}>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            name="possui_piscina"
                                            inputRef={register}
                                            color="primary"
                                        />
                                    }
                                    label="Possui Piscina"
                                    variant="filled"
                                    disabled={allEnabled}
                                />
                            </Grid>
                            <Grid item md sm xs>
                                <FormSelect
                                    id="abastecimento_agua"
                                    name="abastecimento_agua"
                                    label="Abastecimento de Água"
                                    control={control}
                                    inputRef={register}
                                    defaultValue=""
                                    helperText="Porfavor selecione uma opção"
                                    fullWidth
                                    disabled={allEnabled}
                                >
                                    <MenuItem value="" />
                                    <MenuItem value={1}>
                                        Rede encanada até domicílio
                                    </MenuItem>
                                    <MenuItem value={2}>
                                        Poço / Nascente no domicílio
                                    </MenuItem>
                                    <MenuItem value={3}>Cisterna</MenuItem>
                                    <MenuItem value={4}>Carro pipa</MenuItem>
                                    <MenuItem value={5}>Outro</MenuItem>
                                </FormSelect>
                            </Grid>
                            <Grid item md sm xs>
                                <FormSelect
                                    id="agua_consumo_domicilio"
                                    name="agua_consumo_domicilio"
                                    label="Água para Consumo no Domicílio"
                                    inputRef={register}
                                    control={control}
                                    defaultValue=""
                                    helperText="Porfavor selecione uma opção"
                                    fullWidth
                                    disabled={allEnabled}
                                >
                                    <MenuItem value="" />
                                    <MenuItem value={1}>Filtrada</MenuItem>
                                    <MenuItem value={2}>Fervida</MenuItem>
                                    <MenuItem value={3}>Clorada</MenuItem>
                                    <MenuItem value={4}>Mineral</MenuItem>
                                    <MenuItem value={5}>
                                        Sem tratamento
                                    </MenuItem>
                                </FormSelect>
                            </Grid>
                        </Grid>
                        <Grid container spacing={4} alignItems="center">
                            <Grid item md sm xs>
                                <FormSelect
                                    id="forma_escoamento_banheiro"
                                    name="forma_escoamento_banheiro"
                                    label="Forma de Escoamento do Banheiro ou Sanitário"
                                    control={control}
                                    inputRef={register}
                                    defaultValue=""
                                    helperText="Porfavor selecione uma opção"
                                    fullWidth
                                    disabled={allEnabled}
                                >
                                    <MenuItem value="" />
                                    <MenuItem value={1}>
                                        Rede coletora do esgoto ou pluvial
                                    </MenuItem>
                                    <MenuItem value={2}>Fossa séptica</MenuItem>
                                    <MenuItem value={3}>
                                        Fossa rudimentar
                                    </MenuItem>
                                    <MenuItem value={4}>
                                        Direto para um rio, lago ou mar
                                    </MenuItem>
                                    <MenuItem value={5}>Céu aberto</MenuItem>
                                    <MenuItem value={6}>Outra forma</MenuItem>
                                </FormSelect>
                            </Grid>
                            <Grid item md sm xs>
                                <FormSelect
                                    id="destino_lixo"
                                    name="destino_lixo"
                                    label="Destino do Lixo"
                                    inputRef={register}
                                    control={control}
                                    defaultValue=""
                                    helperText="Porfavor selecione uma opção"
                                    fullWidth
                                    disabled={allEnabled}
                                >
                                    <MenuItem value="" />
                                    <MenuItem value={1}>Coletado</MenuItem>
                                    <MenuItem value={2}>
                                        Queimado/Enterrado
                                    </MenuItem>
                                    <MenuItem value={3}>Céu aberto</MenuItem>
                                    <MenuItem value={4}>Outro</MenuItem>
                                </FormSelect>
                            </Grid>
                            <Grid item className={classes.text}>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            inputRef={register}
                                            checked={enableAnimals}
                                            onChange={handleCheckedAnimals}
                                            color="primary"
                                        />
                                    }
                                    name="animais_domicilio"
                                    label="Possui Animais no Domicílio"
                                    variant="filled"
                                    disabled={allEnabled}
                                />
                            </Grid>
                        </Grid>
                        {enableAnimals === true ? (
                            <Grid container spacing={4} alignItems="center">
                                <Grid item md sm xs>
                                    <TextField
                                        name="animais_gatos"
                                        label="Nº de Gatos"
                                        type="number"
                                        inputRef={register}
                                        fullWidth
                                    />
                                </Grid>
                                <Grid item md sm xs>
                                    <TextField
                                        name="animais_caes"
                                        label="Nº de Cães"
                                        type="number"
                                        inputRef={register}
                                        fullWidth
                                    />
                                </Grid>
                                <Grid item md sm xs>
                                    <TextField
                                        name="animais_passaros"
                                        label="Nº de Pássaros"
                                        type="number"
                                        inputRef={register}
                                        fullWidth
                                    />
                                </Grid>
                                <Grid item md sm xs>
                                    <TextField
                                        name="animais_outros"
                                        label="Nº de Outros Animais"
                                        type="number"
                                        inputRef={register}
                                        fullWidth
                                    />
                                </Grid>
                            </Grid>
                        ) : (
                            <> </>
                        )}
                    </>
                ) : immobile === 7 ||
                  immobile === 8 ||
                  immobile === 9 ||
                  immobile === 10 ||
                  immobile === 11 ? (
                    <>
                        <Grid container spacing={4} alignItems="center">
                            <Grid item md sm xs>
                                <Typography variant="h6">
                                    <strong>Condições de Moradia:</strong>
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid container spacing={4} alignItems="center">
                            <Grid item md sm xs>
                                <FormSelect
                                    id="localizacao"
                                    name="localizacao"
                                    label="Localização"
                                    control={control}
                                    defaultValue={location}
                                    onClick={(e) => setLocation(e.target.value)}
                                    fullWidth
                                    required
                                    inputRef={register({
                                        required: !isEnabled,
                                    })}
                                    error={!!errors.localizacao}
                                >
                                    <MenuItem value="" />
                                    <MenuItem value={1}>Urbana</MenuItem>
                                    <MenuItem value={2}>Rural</MenuItem>
                                </FormSelect>
                                {errors.localizacao &&
                                    errors.localizacao.type === 'required' && (
                                        <span className={classes.span}>
                                            Este campo é obrigatório!
                                        </span>
                                    )}
                            </Grid>
                            <Grid item md sm xs>
                                <TextField
                                    name="qtd_moradores"
                                    label="Nº de Moradores"
                                    type="number"
                                    inputRef={register}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item className={classes.text}>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            inputRef={register}
                                            color="primary"
                                        />
                                    }
                                    name="disponibilidade_energia_eletrica"
                                    label="Possui Energia Elétrica"
                                    variant="filled"
                                />
                            </Grid>
                            <Grid item className={classes.text}>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            inputRef={register}
                                            color="primary"
                                        />
                                    }
                                    name="possui_piscina"
                                    label="Possui Piscina"
                                    variant="filled"
                                />
                            </Grid>
                        </Grid>
                        <Grid container spacing={4} alignItems="center">
                            <Grid item md sm xs>
                                <FormSelect
                                    id="abastecimento_agua"
                                    name="abastecimento_agua"
                                    label="Abastecimento de Água"
                                    inputRef={register}
                                    control={control}
                                    defaultValue=""
                                    helperText="Porfavor selecione uma opção"
                                    fullWidth
                                >
                                    <MenuItem value="" />
                                    <MenuItem value={1}>
                                        Rede encanadaaté domicílio
                                    </MenuItem>
                                    <MenuItem value={2}>
                                        Poço / Nascente no domicílio
                                    </MenuItem>
                                    <MenuItem value={3}>Cisterna</MenuItem>
                                    <MenuItem value={4}>Carro pipa</MenuItem>
                                    <MenuItem value={5}>Outro</MenuItem>
                                </FormSelect>
                            </Grid>
                            <Grid item md sm xs>
                                <FormSelect
                                    id="agua_consumo_domicilio"
                                    name="agua_consumo_domicilio"
                                    label="Água para Consumo no Domicílio"
                                    inputRef={register}
                                    control={control}
                                    defaultValue=""
                                    helperText="Porfavor selecione uma opção"
                                    fullWidth
                                >
                                    <MenuItem value="" />
                                    <MenuItem value={1}>Filtrada</MenuItem>
                                    <MenuItem value={2}>Fervida</MenuItem>
                                    <MenuItem value={3}>Clorada</MenuItem>
                                    <MenuItem value={4}>Mineral</MenuItem>
                                    <MenuItem value={5}>
                                        Sem tratamento
                                    </MenuItem>
                                </FormSelect>
                            </Grid>
                            <Grid item md sm xs>
                                <FormSelect
                                    id="forma_escoamento_banheiro"
                                    name="forma_escoamento_banheiro"
                                    label="Forma de Escoamento do Banheiro ou Sanitário"
                                    inputRef={register}
                                    control={control}
                                    defaultValue=""
                                    helperText="Porfavor selecione uma opção"
                                    fullWidth
                                >
                                    <MenuItem value="" />
                                    <MenuItem value={1}>
                                        Rede coletora do esgoto ou pluvial
                                    </MenuItem>
                                    <MenuItem value={2}>Fossa séptica</MenuItem>
                                    <MenuItem value={3}>
                                        Fossa rudimentar
                                    </MenuItem>
                                    <MenuItem value={4}>
                                        Direto para um rio, lago ou mar
                                    </MenuItem>
                                    <MenuItem value={5}>Céu aberto</MenuItem>
                                    <MenuItem value={6}>Outra forma</MenuItem>
                                </FormSelect>
                            </Grid>
                            <Grid item md sm xs>
                                <FormSelect
                                    id="destino_lixo"
                                    name="destino_lixo"
                                    label="Destino do Lixo"
                                    inputRef={register}
                                    control={control}
                                    defaultValue=""
                                    helperText="Porfavor selecione uma opção"
                                    fullWidth
                                >
                                    <MenuItem value="" />
                                    <MenuItem value={1}>Coletado</MenuItem>
                                    <MenuItem value={2}>
                                        Queimado/Enterrado
                                    </MenuItem>
                                    <MenuItem value={3}>Céu aberto</MenuItem>
                                    <MenuItem value={4}>Outro</MenuItem>
                                </FormSelect>
                            </Grid>
                        </Grid>
                    </>
                ) : (
                    <> </>
                )}

                <Grid container spacing={4} alignItems="center">
                    <Grid item className={classes.text}>
                        <FormControlLabel
                            control={
                                <Checkbox inputRef={register} color="primary" />
                            }
                            name="status_termo_recusa"
                            label="Recusa de cadastro"
                            variant="filled"
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <TextField
                            name="anotacao"
                            label="Anotações"
                            multiline
                            rows={4}
                            inputRef={register}
                            variant="outlined"
                        />
                    </Grid>
                </Grid>
            </div>
            <div className={classes.row}>
                <Button variant="contained" color="primary" type="submit">
                    Cadastrar
                </Button>

                <Button
                    variant="contained"
                    color="default"
                    onClick={props.close}
                >
                    Cancelar
                </Button>
            </div>
            <ToastContainer />
        </form>
    );
}
