/* eslint-disable no-undef */
import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Modal,
    Fade,
    Typography,
    Icon,
    Backdrop,
} from '@material-ui/core';
import Pagination from '@material-ui/lab/Pagination';
import VisibilityIcon from '@material-ui/icons/Visibility';
import CancelIcon from '@material-ui/icons/Cancel';
import ViewHomes from '../Forms/viewHomes';
import api from '../../services/api';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    container: {
        maxHeight: '100%',
    },
    header: {
        background: '#dcdcdc',
    },
    icons: {
        textAlign: 'center',
    },
    iconView: {
        marginRight: 10,
        cursor: 'pointer',
    },
    pagination: {
        padding: '0.5rem',
        background: '#dcdcdc',
    },
    row: {
        display: 'flex',
        flexDirection: 'rows',
        justifyContent: 'space-between',
        margin: '2rem',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        width: '95%',
        height: '95%',
        overflowY: 'scroll',
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
}));

export default function StickyHeadTable(props) {
    const classes = useStyles();
    const [page, setPage] = useState(1);
    const [rowsPerPage] = useState(10);
    const [open, setOpen] = useState(false);
    const [totalPages, setTotalPages] = useState(0);
    const [items, setItems] = useState([]);
    const { id } = JSON.parse(sessionStorage.getItem('USERAGENT'));

    async function loadItems() {
        await api
            .get(`domiciliocad?page=${page}&limit=${rowsPerPage}&id=${id}`)
            .then((response) => {
                setTotalPages(Math.ceil(response.data.size / rowsPerPage));

                const arrayPages = [];

                for (let i = 1; i <= totalPages; i++) {
                    arrayPages.push(i);
                }

                setPage(arrayPages);
                setItems(response.data.domicilios);
            });
    }

    useEffect(() => {
        loadItems();
    }, [page]);

    const columns = props.arrayCollum;

    const handleChangePage = (event, value) => {
        setPage(value);
    };

    const handleOpen = (item) => {
        setOpen(true);
        setHomeD(item);
    };

    const handleClose = () => {
        setOpen(false);
    };
    return (
        <>
            <Paper className={classes.root}>
                <TableContainer className={classes.container}>
                    <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <TableRow>
                                {columns.map((column) => (
                                    <TableCell
                                        key={column.id}
                                        className={classes.header}
                                        align={column.align}
                                        style={{ minWidth: column.minWidth }}
                                    >
                                        {column.label}
                                    </TableCell>
                                ))}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {items.map((item) => (
                                <TableRow key={item.id}>
                                    <TableCell>
                                        {item.nome_logradouro}
                                    </TableCell>
                                    <TableCell>{item.numero}</TableCell>
                                    <TableCell>{item.bairro}</TableCell>
                                    <TableCell>{item.microarea}</TableCell>
                                    <TableCell
                                        className={classes.icons}
                                        onClick={() => handleOpen(item.id)}
                                    >
                                        <VisibilityIcon
                                            className={classes.iconView}
                                        />
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <Pagination
                    className={classes.pagination}
                    count={totalPages}
                    color="primary"
                    onChange={handleChangePage}
                    style={{ paddingBottom: 10 }}
                />
            </Paper>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={classes.paper}>
                        <Typography component="h3" className={classes.row}>
                            <strong>Visualizar Domicílio</strong>
                            <Icon color="primary">
                                <CancelIcon
                                    onClick={handleClose}
                                    cursor="pointer"
                                />
                            </Icon>
                        </Typography>
                        <ViewHomes close={handleClose} family="" />
                    </div>
                </Fade>
            </Modal>
        </>
    );
}
