/* eslint-disable no-undef */
import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Pagination from '@material-ui/lab/Pagination';
import VisibilityIcon from '@material-ui/icons/Visibility';

import api from '../../services/api';

const useStyles = makeStyles({
    root: {
        width: '100%',
    },
    container: {
        maxHeight: '100%',
    },
    header: {
        background: '#dcdcdc',
    },
    icons: {
        textAlign: 'center',
    },
    iconView: {
        marginRight: 10,
        cursor: 'pointer',
    },
    pagination: {
        padding: '0.5rem',
        background: '#dcdcdc',
    },
});

export default function StickyHeadTable(props) {
    const classes = useStyles();
    const [page, setPage] = React.useState(1);
    const [rowsPerPage] = React.useState(10);
    const [totalPages, setTotalPages] = React.useState(0);
    const [items, setItems] = React.useState([]);
    const { id } = JSON.parse(sessionStorage.getItem('USERAGENT'));

    async function loadItems() {
        await api
            .get(`cidadaocad?page=${page}&limit=${rowsPerPage}&id=${id}`)
            .then((response) => {
                setTotalPages(Math.ceil(response.data.size / rowsPerPage));

                const arrayPages = [];

                for (let i = 1; i <= totalPages; i++) {
                    arrayPages.push(i);
                }

                setPage(arrayPages);
                setItems(response.data.cidadaos);
            });
    }

    useEffect(() => {
        loadItems();
    }, [page]);

    const columns = props.arrayCollum;

    const handleChangePage = (event, value) => {
        setPage(value);
    };

    return (
        <Paper className={classes.root}>
            <TableContainer className={classes.container}>
                <Table stickyHeader aria-label="sticky table">
                    <TableHead className={classes.header}>
                        <TableRow>
                            {columns.map((column) => (
                                <TableCell
                                    key={column.id}
                                    align={column.align}
                                    style={{ minWidth: column.minWidth }}
                                >
                                    {column.label}
                                </TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {items.map((item) => (
                            <TableRow key={item.id}>
                                <TableCell>{item.nome_cidadao}</TableCell>
                                <TableCell>{item.nome_mae}</TableCell>
                                <TableCell>{item.nome_pai}</TableCell>
                                <TableCell>{item.data_nascimento}</TableCell>
                                <TableCell>{item.microarea}</TableCell>
                                <TableCell className={classes.icons}>
                                    <VisibilityIcon
                                        className={classes.iconView}
                                    />
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <Pagination
                className={classes.pagination}
                count={totalPages}
                color="primary"
                onChange={handleChangePage}
                style={{ paddingBottom: 10 }}
            />
        </Paper>
    );
}
