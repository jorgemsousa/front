/* eslint-disable no-undef */
import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
} from '@material-ui/core';
import Pagination from '@material-ui/lab/Pagination';
import VisibilityIcon from '@material-ui/icons/Visibility';

import api from '../../services/api';

const useStyles = makeStyles({
    root: {
        width: '100%',
    },
    container: {
        maxHeight: '100%',
    },
    header: {
        background: '#dcdcdc',
    },
    icons: {
        textAlign: 'center',
    },
    iconView: {
        marginRight: 10,
        cursor: 'pointer',
    },
    pagination: {
        padding: '0.5rem',
        background: '#dcdcdc',
    },
});

export default function StickyHeadTable(props) {
    const classes = useStyles();
    const [page, setPage] = React.useState(1);
    const [rowsPerPage] = React.useState(10);
    const [totalPages, setTotalPages] = React.useState(0);
    const [items, setItems] = React.useState([]);
    const { id } = JSON.parse(sessionStorage.getItem('USERAGENT'));

    async function loadItems() {
        await api
            .get(`domiciliocad?page=${page}&limit=${rowsPerPage}&id=${id}`)
            .then((response) => {
                setTotalPages(Math.ceil(response.data.size / rowsPerPage));

                const arrayPages = [];

                for (let i = 1; i <= totalPages; i++) {
                    arrayPages.push(i);
                }

                setPage(arrayPages);
                setItems(response.data.domicilios);
            });
    }

    useEffect(() => {
        loadItems();
    }, [page]);

    const columns = props.arrayCollum;

    const handleChangePage = (event, value) => {
        setPage(value);
    };

    return (
        <>
            <Paper className={classes.root}>
                <TableContainer className={classes.container}>
                    <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <TableRow>
                                {columns.map((column) => (
                                    <TableCell
                                        key={column.id}
                                        className={classes.header}
                                        align={column.align}
                                        style={{ minWidth: column.minWidth }}
                                    >
                                        {column.label}
                                    </TableCell>
                                ))}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {items.map((item) => (
                                <TableRow key={item.id}>
                                    <TableCell>
                                        {item.nome_logradouro}
                                    </TableCell>
                                    <TableCell>{item.numero}</TableCell>
                                    <TableCell>{item.bairro}</TableCell>
                                    <TableCell>{item.microarea}</TableCell>
                                    <TableCell className={classes.icons}>
                                        <VisibilityIcon
                                            className={classes.iconView}
                                        />
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <Pagination
                    className={classes.pagination}
                    count={totalPages}
                    color="primary"
                    onChange={handleChangePage}
                    style={{ paddingBottom: 10 }}
                />
            </Paper>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={classes.paper}>
                        <Typography component="h3" className={classes.row}>
                            <strong>Visualizar Familia</strong>
                            <Icon color="primary">
                                <CancelIcon
                                    onClick={handleClose}
                                    cursor="pointer"
                                />
                            </Icon>
                        </Typography>
                        <ViewFormFamily close={handleClose} family={familyID} />
                    </div>
                </Fade>
            </Modal>
        </>
    );
}
