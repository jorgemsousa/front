import FormControl from '@material-ui/core/FormControl';
import { TextField } from '@material-ui/core';
import { Controller } from 'react-hook-form';

const FormSelect = (props) => {
    const { name, label, control, defaultValue, children } = props;
    return (
        <FormControl {...props}>
            <Controller
                as={
                    <TextField
                        label={label}
                        required={props.required}
                        inputRef={props.inputRef}
                        error={props.error}
                        value={props.value}
                    >
                        {children}
                    </TextField>
                }
                select
                name={name}
                control={control}
                defaultValue={props.value}
                onChange={([selected]) =>
                    // React Select return object instead of value for selection
                    ({ value: selected })
                }
                disabled={props.disabled}
                rules={{ required: true }}
            />
        </FormControl>
    );
};
export default FormSelect;
