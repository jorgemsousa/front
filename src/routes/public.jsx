import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Dashboard from '../pages/dashboard/index';
import Homes from '../pages/registrations/homes/index';
import Individuals from '../pages/registrations/individuals/index';
import Families from '../pages/registrations/families/index';
import Visits from '../pages/registrations/visits/index';
import Error404 from '../pages/Error/404';
import Login from '../pages/Login/index';
import Perfil from '../pages/user/index';
import LoginAgent from '../pages/Login/indexAgent';
import Ihomes from '../pages/inconsistency/homes';
import Iindividuals from '../pages/inconsistency/individuals';
import Partial from '../pages/partial';

import PrivateRoutes from './private';

const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1,
    },
}));

export default function CenteredGrid() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Switch>
                <Route exact path="/" component={Login} />

                <PrivateRoutes path="/partial" component={Partial} />
                <PrivateRoutes path="/loginAgent" component={LoginAgent} />
                <PrivateRoutes path="/perfil" component={Perfil} />
                <PrivateRoutes path="/dashboard" component={Dashboard} />
                <PrivateRoutes path="/rhomes" component={Homes} />
                <PrivateRoutes path="/rindividuals" component={Individuals} />
                <PrivateRoutes path="/rfamilies" component={Families} />
                <PrivateRoutes path="/rvisits" component={Visits} />
                <PrivateRoutes path="/ihomes" component={Ihomes} />
                <PrivateRoutes path="/iindividuals" component={Iindividuals} />
                <PrivateRoutes path="/*" component={Error404} />
            </Switch>
        </div>
    );
}
