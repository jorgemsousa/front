/* eslint-disable no-undef */
import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';

import { isAuthenticated } from '../services/auth';
import Partial from '../pages/partial';
import Agent from '../pages/Login/indexAgent';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        backgroundColor: '#EEEEEE',
        paddingBottom: 30,
        marginBottom: 20,
        height: '100vh',
    },
    nested: {
        paddingLeft: theme.spacing(4),
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9) + 1,
        },
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    logo: {
        width: 150,
        height: 60,
        paddingTop: 15,
    },
    span: {
        fontSize: 14,
        marginRight: 10,
    },
    user: {
        display: 'inline',
        padding: '9px 13px',
        marginRight: 10,
        backgroundColor: '#EE1289',
        border: '1px solid #fff',
    },
}));

export default function PrivateRoute({ component: Component, ...rest }) {
    const classes = useStyles();
    const USERAGENT = sessionStorage.getItem('USERAGENT');
    return (
        <Route
            {...rest}
            render={(props) =>
                isAuthenticated() ? (
                    <div className={classes.root}>
                        {USERAGENT === null || USERAGENT === undefined ? (
                            <Agent />
                        ) : (
                            <>
                                <Partial />
                                <main className={classes.content}>
                                    <div className={classes.toolbar} />
                                    <Component {...props} />
                                </main>
                            </>
                        )}
                    </div>
                ) : (
                    <Redirect
                        to={{ pathname: '/', state: { from: props.location } }}
                    />
                )
            }
        />
    );
}
