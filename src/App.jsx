import { BrowserRouter } from 'react-router-dom';
import PublicRoutes from './routes/public';

import './styles/global.css';

function App() {
    return (
        <BrowserRouter>
            <div className="App">
                <PublicRoutes />
            </div>
        </BrowserRouter>
    );
}

export default App;
