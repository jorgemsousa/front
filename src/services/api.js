/* eslint-disable camelcase */
import axios from 'axios';
import { getToken, logout } from './auth';

const api = axios.create({
    baseURL: 'http://localhost:3333/',
});

api.interceptors.request.use(
    async (config) => {
        const token = getToken();
        if (token) {
            config.headers.Authorization = `Bearer ${token}`;
        }
        return config;
    },
    (error) => Promise.reject(error)
);

api.interceptors.response.use(
    (response) => Promise.resolve(response),
    (error) => {
        if (error.response.status === 401) {
            logout();
            window.location = '/';
        }
        return Promise.reject(error.response.data);
    }
);

export default api;
