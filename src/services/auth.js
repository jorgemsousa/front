/* eslint-disable no-restricted-globals */
/* eslint-disable no-undef */

export const isAuthenticated = () =>
    sessionStorage.getItem('TOKEN_KEY') !== null;
export const getToken = () => sessionStorage.getItem('TOKEN_KEY');
export const login = (token, user) => {
    sessionStorage.setItem('TOKEN_KEY', token);
    sessionStorage.setItem('USER', JSON.stringify(user));
};
export const getAgent = (userAgent) => {
    sessionStorage.setItem('USERAGENT', JSON.stringify(userAgent));
};
export const getLogin = (userName) => {
    localStorage.setItem('USERNAME', JSON.stringify(userName));
};
export const removeLogin = () => {
    localStorage.removeItem('USERNAME');
};
export const logout = () => {
    sessionStorage.clear();
    location.href = '/';
};
