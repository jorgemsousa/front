import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DashboardIcon from '@material-ui/icons/Dashboard';
import ViewListIcon from '@material-ui/icons/ViewList';
import PhonelinkEraseIcon from '@material-ui/icons/PhonelinkErase';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Collapse from '@material-ui/core/Collapse';
import HomeIcon from '@material-ui/icons/Home';
import PersonIcon from '@material-ui/icons/Person';
import PeopleIcon from '@material-ui/icons/People';
import HomeWorkIcon from '@material-ui/icons/HomeWork';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    nested: {
        paddingLeft: theme.spacing(4),
    },
    link: {
        textDecoration: 'none',
        color: '#4F4F4F',
        hover: {
            color: '#4F4F4F',
        },
    },
}));

export default function MiniDrawer() {
    const classes = useStyles();
    const [menuRegister, setMenuRegister] = React.useState(false);
    const [menuInconsists, setMenuInconsists] = React.useState(false);
    const [selectedIndex, setSelectedIndex] = React.useState(1);

    const handleListItemClick = (event, index) => {
        setSelectedIndex(index);
    };

    const handleRegister = () => {
        setMenuRegister(!menuRegister);
    };

    const handleInconsists = () => {
        setMenuInconsists(!menuInconsists);
    };

    return (
        <List>
            <Link to="/dashboard" className={classes.link}>
                <ListItem
                    button
                    selected={selectedIndex === 0}
                    onClick={(event) => {
                        handleListItemClick(event, 0);
                    }}
                >
                    <ListItemIcon>
                        <DashboardIcon />
                    </ListItemIcon>
                    <ListItemText primary="Dashboard" />
                </ListItem>
            </Link>
            <Divider />
            <ListItem button onClick={handleRegister}>
                <ListItemIcon>
                    <ViewListIcon />
                </ListItemIcon>
                <ListItemText primary="Cadastros" />
                {menuRegister ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={menuRegister} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                    <Link to="/rhomes" className={classes.link}>
                        <ListItem
                            button
                            className={classes.nested}
                            selected={selectedIndex === 1}
                            onClick={(event) => handleListItemClick(event, 1)}
                        >
                            <ListItemIcon>
                                <HomeIcon />
                            </ListItemIcon>
                            <ListItemText primary="Domicílios" />
                        </ListItem>
                    </Link>
                </List>
                <List component="div" disablePadding>
                    <Link to="/rindividuals" className={classes.link}>
                        <ListItem
                            button
                            className={classes.nested}
                            selected={selectedIndex === 2}
                            onClick={(event) => handleListItemClick(event, 2)}
                        >
                            <ListItemIcon>
                                <PersonIcon />
                            </ListItemIcon>
                            <ListItemText primary="Indivíduos" />
                        </ListItem>
                    </Link>
                </List>
                <List component="div" disablePadding>
                    <Link to="/rfamilies" className={classes.link}>
                        <ListItem
                            button
                            className={classes.nested}
                            selected={selectedIndex === 3}
                            onClick={(event) => handleListItemClick(event, 3)}
                        >
                            <ListItemIcon>
                                <PeopleIcon />
                            </ListItemIcon>
                            <ListItemText primary="Famílias" />
                        </ListItem>
                    </Link>
                </List>
                <List component="div" disablePadding>
                    <Link to="/rvisits" className={classes.link}>
                        <ListItem
                            button
                            className={classes.nested}
                            selected={selectedIndex === 4}
                            onClick={(event) => handleListItemClick(event, 4)}
                        >
                            <ListItemIcon>
                                <HomeWorkIcon />
                            </ListItemIcon>
                            <ListItemText primary="Visítas" />
                        </ListItem>
                    </Link>
                </List>
            </Collapse>

            <ListItem button onClick={handleInconsists}>
                <ListItemIcon>
                    <PhonelinkEraseIcon />
                </ListItemIcon>
                <ListItemText primary="Inconsistências" />
                {menuInconsists ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={menuInconsists} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                    <Link to="/ihomes" className={classes.link}>
                        <ListItem
                            button
                            className={classes.nested}
                            selected={selectedIndex === 6}
                            onClick={(event) => handleListItemClick(event, 6)}
                        >
                            <ListItemIcon>
                                <HomeIcon />
                            </ListItemIcon>
                            <ListItemText primary="Domicílios" />
                        </ListItem>
                    </Link>
                </List>
                <List component="div" disablePadding>
                    <Link to="/iindividuals" className={classes.link}>
                        <ListItem
                            button
                            className={classes.nested}
                            selected={selectedIndex === 5}
                            onClick={(event) => handleListItemClick(event, 5)}
                        >
                            <ListItemIcon>
                                <PersonIcon />
                            </ListItemIcon>
                            <ListItemText primary="Indivíduos" />
                        </ListItem>
                    </Link>
                </List>
            </Collapse>
        </List>
    );
}
