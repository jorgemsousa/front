/* eslint-disable no-prototype-builtins */
import FormControl from '@material-ui/core/FormControl';
import { TextField } from '@material-ui/core';
import { Controller, useFormContext } from 'react-hook-form';

const FormSelect = (props) => {
    const { control } = useFormContext();
    const { name, label, errorobj, children, defaultValue } = props;
    let isError = false;
    let errorMessage = '';
    if (errorobj && errorobj.hasOwnProperty(name)) {
        isError = true;
        errorMessage = errorobj[name].message;
    }
    return (
        <FormControl {...props}>
            <Controller
                as={
                    <TextField
                        label={label}
                        required={props.required}
                        inputRef={props.inputRef}
                        error={props.error}
                        helperText={errorMessage}
                    >
                        {children}
                    </TextField>
                }
                select
                name={name}
                control={control}
                defaultValue={defaultValue}
                onChange={([selected]) =>
                    // React Select return object instead of value for selection
                    ({ value: selected })
                }
                disabled={props.disabled}
                error={isError}
            />
        </FormControl>
    );
};
export default FormSelect;
