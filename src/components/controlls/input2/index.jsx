/* eslint-disable no-prototype-builtins */
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import { useForm, Controller } from 'react-hook-form';

const FormInput = (props) => {
    const { control } = useForm();
    const { name, label, required, errorobj } = props;
    const isError = required;
    let errorMessage = '';
    let isTrue = false;
    if (isError) {
        if (errorobj && errorobj.hasOwnProperty(name)) {
            errorMessage = errorobj[name].message;
            isTrue = true;
        }
    }
    return (
        <FormControl {...props}>
            <Controller
                as={TextField}
                name={name}
                control={control}
                defaultValue=""
                label={label}
                fullWidth
                InputLabelProps={{
                    className: required ? 'required-label' : '',
                    required: required || false,
                }}
                error={isTrue}
                helperText={errorMessage}
                {...props}
            />
        </FormControl>
    );
};
export default FormInput;
