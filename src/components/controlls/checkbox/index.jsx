import React from 'react';
import { useFormContext, Controller } from 'react-hook-form';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

const MuiCheckbox = (props) => {
    const { label, name } = props;
    return (
        <FormControlLabel
            control={<Checkbox name={name} value={name} {...props} />}
            label={label}
        />
    );
};

function FormCheckBox(props) {
    const { control } = useFormContext();
    const { name, label, value } = props;
    return (
        <>
            <Controller
                as={MuiCheckbox}
                name={name}
                checked={value}
                control={control}
                onChange={([e]) => (e.target.checked ? e.target.value : '')}
                defaultValue={false}
                label={label}
                {...props}
            />
        </>
    );
}

export default FormCheckBox;
