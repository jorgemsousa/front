import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Card, CardHeader, CardContent, Typography } from '@material-ui/core';

const useStyles = makeStyles(() => ({
    root: {
        maxWidth: 345,
    },
    content: {
        textAlign: 'center',
        fontSize: 22,
        height: 100,
        paddingTop: 40,
    },

    header: {
        fontFamily: 'Arial',
        textAlign: 'center',
        color: '#FFFFFF',
    },
}));

export default function CardIMC(props) {
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardHeader
                className={classes.header}
                title={props.title}
                style={{ backgroundColor: props.bcolor }}
            />
            <CardContent
                className={classes.content}
                style={{ backgroundColor: props.bcontentcolor }}
            >
                <Typography color="000">{props.text}</Typography>
            </CardContent>
        </Card>
    );
}
