import React from 'react';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Fab from '@material-ui/core/Fab';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
    root: {
        minWidth: 275,
        minHeight: 220,
        marginTop: 80,
        backgroundColor: '#F8F8FF',
    },
    bullet: {
        position: 'relative',
        marginLeft: '80%',
        margin: '20px 2px',
        transform: 'scale(1.2)',
        marginBottom: -20,
    },
    text: {
        fontSize: 16,
        marginTop: 80,
    },
    pos: {
        position: 'absolute',
        marginTop: -100,
        marginBottom: 12,
        width: 150,
        height: 120,
        paddingTop: 30,
        borderRadius: 10,
        color: '#fff',
        textAlign: 'center',
        alignItems: 'center',
    },
    link: {
        textDecoration: 'none',
    },
});

export default function SimpleCard(props) {
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardContent>
                <Link className={classes.link} to={props.link}>
                    <Fab color="secondary" className={classes.bullet}>
                        {props.icon}
                    </Fab>
                </Link>

                <Paper
                    className={classes.pos}
                    style={{ backgroundColor: `${props.color}` }}
                >
                    <Typography variant="h3" gutterBottom>
                        <strong>{props.data}</strong>
                    </Typography>
                </Paper>
                <Typography
                    variant="body2"
                    component="p"
                    className={classes.text}
                >
                    {props.text}
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="large">
                    <strong>{props.type}</strong>
                </Button>
            </CardActions>
        </Card>
    );
}
