import React from 'react';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import DoneIcon from '@material-ui/icons/Done';

const useStyles = makeStyles({
    root: {
        marginTop: 30,
        backgroundColor: '#FAFAFA',
    },
    bullet: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    text: {
        fontSize: 18,
        textAlign: 'left',
        zIndex: 2,
        color: '#000',
    },
    data: {
        fontSize: 20,
        fontWeight: 'bolder',
    },
    hr: {
        border: '1px solid #cacaca',
        marginTop: 30,
        marginBottom: 5,
    },
    link: {
        textDecoration: 'none',
    },
});

export default function SimpleCard(props) {
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardContent className={classes.bullet}>
                <Typography
                    variant="body2"
                    component="p"
                    className={classes.text}
                >
                    {props.text}
                </Typography>
                <Chip
                    className={classes.data}
                    label={props.data}
                    clickable
                    color="primary"
                    deleteIcon={<DoneIcon />}
                />
            </CardContent>
            <CardActions>
                <Typography
                    variant="body2"
                    component="p"
                    className={classes.text}
                >
                    <Link className={classes.link} to={props.link}>
                        <Button size="small">Ver Todos</Button>
                    </Link>
                </Typography>
            </CardActions>
        </Card>
    );
}
