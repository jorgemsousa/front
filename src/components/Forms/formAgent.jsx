/* eslint-disable no-restricted-globals */
/* eslint-disable no-alert */
/* eslint-disable no-undef */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-use-before-define */
import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Grid, TextField, Button, makeStyles } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import Autocomplete from '@material-ui/lab/Autocomplete';
import api from '../../services/api';
import 'react-toastify/dist/ReactToastify.css';
import { getAgent } from '../../services/auth';

function sleep(delay = 0) {
    return new Promise((resolve) => {
        setTimeout(resolve, delay);
    });
}
const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '100%',
            height: '100%',
        },
    },
    containner: {
        border: '1px solid #c1c1c1',
        borderRadius: '7px',
        margin: 'auto',
        marginTop: '10%',
        justifyContent: 'space-between',
        padding: '4rem',
        maxWidth: '50rem',
    },
    row: {
        display: 'flex',
        flexDirection: 'rows',
        justifyContent: 'space-between',
    },
    group: {
        padding: '0.5rem',
        border: '1px solid #c1c1c1',
        borderRadius: 8,
        marginTop: 5,
        marginBottom: 5,
    },
}));

export default function FormAgent(props) {
    const classes = useStyles();
    const history = useHistory();

    const [openUnity, setOpenUnity] = useState(false);
    const [openTeam, setOpenTeam] = useState(false);
    const [openAgent, setOpenAgent] = useState(false);
    const [isEnabled, setIsEnabled] = useState(true);
    const [unityId, setUnityId] = useState('0');
    const [teamId, setTeamId] = useState('0');
    const [agentId, setAgentId] = useState('0');
    const [options, setOptions] = useState([]);
    const [optionsTeams, setOptionsTeams] = useState([]);
    const [optionsAgent, setOptionsAgent] = useState([]);
    const loading = openUnity && options.length === 0;
    const loadingTeam = openTeam && optionsTeams.length === 0;
    const loadingAgent = openAgent && optionsAgent.length === 0;

    const handleGetAgent = async (e) => {
        e.preventDefault();
        const response = await api.post(
            `/getagent?id_agent=${agentId.id_pessoa}`
        );
        getAgent(response.data.userAgent);
        history.push('/dashboard');
    };

    useEffect(() => {
        let active = true;

        if (!loading) {
            return undefined;
        }

        (async () => {
            const response = await api.get('/unity');
            await sleep(1e3); // For demo purposes.
            if (active) {
                setOptions(response.data.unity);
            }
        })();

        return () => {
            active = false;
        };
    }, [loading]);

    useEffect(async () => {
        const { id } = unityId;
        if (id === '0') {
            return;
        }
        const response = await api.get(`/teams?id=${id}`);
        const teams = response.data;
        setOptionsTeams(teams.team);
    }, [unityId]);

    useEffect(async () => {
        const { id } = teamId;
        if (id === '0') {
            return;
        }
        const response = await api.get(
            `/clinicalStaff?id_team=${id}&&id_unity=${unityId.id}`
        );
        setOptionsAgent(response.data.ClinicalStaff);
    }, [teamId]);

    useEffect(() => {
        if (!openUnity) {
            setOptions([]);
        }
    }, [openUnity]);

    useEffect(() => {
        if (!openTeam) {
            setOptions([]);
        }
    }, [openTeam]);

    useEffect(() => {
        if (!openAgent) {
            setOptionsAgent([]);
        }
    }, [openAgent]);

    return (
        <div className={classes.containner}>
            <form onSubmit={handleGetAgent}>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <Autocomplete
                            name="unity"
                            fullWidth
                            open={openUnity}
                            onOpen={() => {
                                setOpenUnity(true);
                            }}
                            onClose={() => {
                                setOpenUnity(false);
                            }}
                            getOptionSelected={(option, value) =>
                                option.nome === value.nome
                            }
                            getOptionLabel={(option) => option.nome}
                            options={options}
                            loading={loading}
                            onChange={(event, value) => {
                                setUnityId(value);
                            }}
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    name="unity"
                                    label="Unidade de Atendimento"
                                    variant="outlined"
                                    fullWidth
                                    required
                                    InputProps={{
                                        ...params.InputProps,
                                        endAdornment: (
                                            <>
                                                {loading ? (
                                                    <CircularProgress
                                                        color="red"
                                                        size={20}
                                                    />
                                                ) : null}
                                                {params.InputProps.endAdornment}
                                            </>
                                        ),
                                    }}
                                />
                            )}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <Autocomplete
                            id="team"
                            fullWidth
                            open={openTeam}
                            onOpen={() => {
                                setOpenTeam(true);
                            }}
                            onClose={() => {
                                setOpenTeam(false);
                            }}
                            getOptionSelected={(option, value) =>
                                option.no_equipe === value.no_equipe
                            }
                            getOptionLabel={(option) => option.no_equipe}
                            options={optionsTeams}
                            loading={loadingTeam}
                            onChange={(event, value) => {
                                setTeamId(value);
                            }}
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    label="Equipe de Atendimento"
                                    variant="outlined"
                                    onChange={(event, value) =>
                                        setTeamId(value)
                                    }
                                    fullWidth
                                    required
                                    InputProps={{
                                        ...params.InputProps,
                                        endAdornment: (
                                            <>
                                                {loadingTeam ? (
                                                    <CircularProgress
                                                        color="inherit"
                                                        size={20}
                                                    />
                                                ) : null}
                                                {params.InputProps.endAdornment}
                                            </>
                                        ),
                                    }}
                                />
                            )}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <Autocomplete
                            id="agent"
                            fullWidth
                            open={openAgent}
                            onOpen={() => {
                                setOpenAgent(true);
                            }}
                            onClose={() => {
                                setOpenAgent(false);
                            }}
                            getOptionSelected={(option, value) =>
                                option.tb_usuario.nome === value.tb_usuario.nome
                            }
                            getOptionLabel={(option) => option.tb_usuario.nome}
                            options={optionsAgent}
                            loading={loadingAgent}
                            onChange={(event, value) => {
                                setAgentId(value);
                                setIsEnabled(false);
                            }}
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    label="Agente de Saúde"
                                    variant="outlined"
                                    fullWidth
                                    required
                                    InputProps={{
                                        ...params.InputProps,
                                        endAdornment: (
                                            <>
                                                {loadingAgent ? (
                                                    <CircularProgress
                                                        color="inherit"
                                                        size={20}
                                                    />
                                                ) : null}
                                                {params.InputProps.endAdornment}
                                            </>
                                        ),
                                    }}
                                />
                            )}
                        />
                    </Grid>
                </Grid>

                <Grid
                    container
                    spacing={4}
                    justify="space-between"
                    alignItems="center"
                >
                    <Grid
                        container
                        justify="center"
                        style={{ marginTop: '3rem', marginBottom: '20px' }}
                    >
                        <Button
                            onClick={props.close}
                            variant="contained"
                            type="submit"
                            color="primary"
                            style={{ textTransform: 'none', width: '30rem' }}
                            disabled={isEnabled}
                        >
                            Selecionar
                        </Button>
                    </Grid>
                </Grid>
            </form>
        </div>
    );
}
