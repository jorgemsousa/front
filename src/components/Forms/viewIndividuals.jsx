/* eslint-disable no-alert */
/* eslint-disable no-undef */
import React, { useState, useEffect, Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Grid,
    Typography,
    TextField,
    CircularProgress,
    MenuItem,
    FormControlLabel,
    Checkbox,
    Button,
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { mask, unMask } from 'remask';
import { useForm, Controller } from 'react-hook-form';
import ImcsCards from '../Imcs';
import api from '../../services/api';
import FormSelect from '../controlls/select/index';

function sleep(delay = 0) {
    return new Promise((resolve) => {
        setTimeout(resolve, delay);
    });
}

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '100%',
            height: '100%',
        },
    },
    container: {
        border: '1px solid #c1c1c1',
        borderRadius: '7px',
        margin: 'auto',
        marginTop: 10,
        justifyContent: 'space-between',
        padding: 20,
        overflowY: 'auto',
    },
    row: {
        display: 'flex',
        flexDirection: 'rows',
        justifyContent: 'space-between',
        margin: '2rem',
    },
    group: {
        padding: '0.5rem',
        border: '1px solid #c1c1c1',
        borderRadius: 8,
        marginTop: 5,
        marginBottom: 5,
    },
    span: {
        display: 'flex',
        color: '#FF0000',
        fontSize: 11,
        marginLeft: 8,
        marginTop: -5,
    },
}));

export default function FormIndividuals(props) {
    const classes = useStyles();
    const { handleSubmit, errors, register, control } = useForm();
    const user = JSON.parse(sessionStorage.getItem('USERAGENT'));

    const [open, setOpen] = useState(false);
    const [options, setOptions] = useState([]);
    const [families, setFamilies] = useState([]);
    const [disableMicroArea, setDisableMicroArea] = useState(false);
    const [isDisabledMother, setIsDisabledMother] = useState(false);
    const [isDisabledDad, setIsDisabledDad] = useState(false);
    const [isDisabled] = useState(false);
    const [peso, setPeso] = useState('');
    const [altura, setAltura] = useState('');
    const [valueCPF, setValueCPF] = useState('');
    const [dateText, setDateText] = useState('');
    const [phone, setPhone] = useState('');
    const [responsavel, setResponsavel] = useState('');
    const [nationality, setNationality] = useState(0);
    const [sex, setSex] = useState(true);
    const [genre, setGenre] = useState(true);
    const [peopleCommunity, setPeopleCommunity] = useState(true);
    const [selectSex, setSelectSex] = useState(0);
    const [maternity, setMaternity] = useState(true);
    const [hasCancer, setHasCancer] = useState(false);
    const [hadCancer, setHadCancer] = useState(false);
    const [hasDiabetes, setHasDiabetes] = useState(false);
    const [hasHospitalization, setHasHospitalization] = useState(true);
    const [medicinalPlants, setMedicinalPlants] = useState(true);
    const [haddisease, setHaddisease] = useState(false);
    const [kidneys, setKidneys] = useState(false);
    const [repiratory, setRespiratory] = useState(false);
    const [homeless, setHomeless] = useState(false);
    const [institution, setInstitution] = useState(true);
    const [kinship, setKinship] = useState(true);

    const loading = open && options.length === 0;

    useEffect(() => {
        let active = true;

        if (!loading) {
            return undefined;
        }

        (async () => {
            const response = await fetch(
                'https://restcountries.eu/rest/v2/all'
            );
            await sleep(1e3); // For demo purposes.
            const countries = await response.json();
            if (active) {
                setOptions(
                    Object.keys(countries).map((key) => countries[key].item[0])
                );
            }
        })();

        return () => {
            active = false;
        };
    }, [loading]);

    useEffect(async () => {
        await api.get(`/cidadaos/familias?id=${user.id}`).then((response) => {
            setFamilies(response.data.cidadaos);
        });
    }, []);

    useEffect(() => {
        if (!open) {
            setOptions([]);
        }
    }, [open]);

    const onSubmit = (data) => {
        alert(JSON.stringify(data));
    };

    /*  const handleDisabled = () => {
        if (isDisabled === false) {
            setIsDisabled(true);
        } else {
            setIsDisabled(false);
        }
    }; */
    const handleDisabledMother = () => {
        if (isDisabledMother === false) {
            setIsDisabledMother(true);
        } else {
            setIsDisabledMother(false);
        }
    };
    const handleDisabledDad = () => {
        if (isDisabledDad === false) {
            setIsDisabledDad(true);
        } else {
            setIsDisabledDad(false);
        }
    };

    const onchangeCPF = (ev) => {
        setValueCPF(mask(unMask(ev.target.value), ['999.999.999-99']));
    };
    const onchangeDate = (ev) => {
        setDateText(mask(unMask(ev.target.value), ['99/99/9999']));
    };
    const onchangePhone = (ev) => {
        setPhone(
            mask(unMask(ev.target.value), [
                '(99) 9999-9999',
                '(99) 9 9999-9999',
            ])
        );
    };

    const handleNationality = (event) => {
        setNationality(event.target.value);
    };

    const disableSex = () => {
        if (sex === false) {
            setSex(true);
        } else {
            setSex(false);
        }
    };
    const disableGenre = () => {
        if (genre === false) {
            setGenre(true);
        } else {
            setGenre(false);
        }
    };

    const handlePeopleCommunity = () => {
        if (peopleCommunity === false) {
            setPeopleCommunity(true);
        } else {
            setPeopleCommunity(false);
        }
    };

    const handleSelectSex = (event) => {
        setSelectSex(event.target.value);
    };

    const disabledMaternity = () => {
        if (maternity === false) {
            setMaternity(true);
        } else {
            setMaternity(false);
        }
    };

    const disabledHasCancer = () => {
        if (hasCancer === false) {
            setHasCancer(true);
        } else {
            setHasCancer(false);
        }
    };
    const disabledHadCancer = () => {
        if (hadCancer === false) {
            setHadCancer(true);
        } else {
            setHadCancer(false);
        }
    };
    const disabledHasDiabetes = () => {
        if (hasDiabetes === false) {
            setHasDiabetes(true);
        } else {
            setHasDiabetes(false);
        }
    };
    const disabledHasHospitalization = () => {
        if (hasHospitalization === false) {
            setHasHospitalization(true);
        } else {
            setHasHospitalization(false);
        }
    };
    const disabledMedicinalPlants = () => {
        if (medicinalPlants === false) {
            setMedicinalPlants(true);
        } else {
            setMedicinalPlants(false);
        }
    };
    const hadheartdisease = () => {
        if (haddisease === false) {
            setHaddisease(true);
        } else {
            setHaddisease(false);
        }
    };
    const hashadKidneys = () => {
        if (kidneys === false) {
            setKidneys(true);
        } else {
            setKidneys(false);
        }
    };
    const haveRespiratoryDisease = () => {
        if (repiratory === false) {
            setRespiratory(true);
        } else {
            setRespiratory(false);
        }
    };
    const haveHomeless = () => {
        if (homeless === false) {
            setHomeless(true);
        } else {
            setHomeless(false);
        }
    };
    const hasInstitution = () => {
        if (institution === false) {
            setInstitution(true);
        } else {
            setInstitution(false);
        }
    };
    const hasKinship = () => {
        if (kinship === false) {
            setKinship(true);
        } else {
            setKinship(false);
        }
    };

    return (
        <form
            className={classes.root}
            onSubmit={handleSubmit(onSubmit)}
            noValidate
            autoComplete="off"
        >
            <div className={classes.container}>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <Controller
                            render={({ onChange }) => (
                                <Autocomplete
                                    options={families}
                                    getOptionLabel={(option) =>
                                        `1 - ${option.nome_cidadao}`
                                    }
                                    renderOption={(option) =>
                                        `1 - ${option.nome_cidadao}`
                                    }
                                    getOptionSelected={(option) => {
                                        setResponsavel(option);
                                    }}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            label="Família"
                                            required
                                            error={
                                                !!errors.cadastro_domicilio_uuid
                                            }
                                        />
                                    )}
                                    onChange={(e) => onChange(e.target.value)}
                                    {...props}
                                />
                            )}
                            onChange={([, data]) =>
                                onChange(data.uuid_ficha_originadora)
                            }
                            defaultValue={options.uuid_ficha_originadora}
                            name="familia_id"
                            control={control}
                            rules={{ required: true }}
                            inputRef={register({
                                required: true,
                            })}
                            required
                            error={!!errors.familia_id}
                        />
                        {errors.familia_id &&
                            errors.familia_id.type === 'required' && (
                                <span className={classes.span}>
                                    Este campo é obrigatório!
                                </span>
                            )}
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            name="cns_responsavel_familiar"
                            label="Responsável Familiar"
                            fullWidth
                            disabled
                            inputRef={register}
                            onChange={responsavel.nome_cidadao || ''}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            name="cns_cidadao"
                            label="Nº do Cartão SUS"
                            type="number"
                            fullWidth
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="status_responsavel"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Responsável Familiar"
                            variant="filled"
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <TextField
                            name="nome_cidadao"
                            label="Nome Completo"
                            fullWidth
                            required
                            inputRef={register({ required: true })}
                            error={!!errors.nome}
                        />
                        {errors.nome && errors.nome.type === 'required' && (
                            <span className={classes.span}>
                                Este campo é obrigatório!
                            </span>
                        )}
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            name="nr_nis"
                            label="Nº do Prontuário na UBS"
                            type="number"
                            fullWidth
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            disabled={disableMicroArea}
                            name="microarea"
                            label="Micro-área"
                            inputProps={{ maxLength: 2 }}
                            inputRef={register({ required: !disableMicroArea })}
                            fullWidth
                            required
                            error={!!errors.microarea}
                        />
                        {errors.microarea &&
                            errors.microarea.type === 'required' && (
                                <span className={classes.span}>
                                    Este campo é obrigatório!
                                </span>
                            )}
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() =>
                                        setDisableMicroArea(!disableMicroArea)
                                    }
                                    name="st_fora_area"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Fora da área"
                            variant="filled"
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <TextField
                            name="cpf"
                            label="CPF"
                            type="text"
                            fullWidth
                            onChange={onchangeCPF}
                            value={valueCPF}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            name="rg"
                            label="RG"
                            fullWidth
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            name="nome_social"
                            label="Nome Social"
                            fullWidth
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            name="data_nascimento"
                            label="Data de Nascimento"
                            type="text"
                            fullWidth
                            required
                            onChange={onchangeDate}
                            value={dateText}
                            inputRef={register({ required: true })}
                            error={!!errors.nascimento}
                        />
                        {errors.nascimento &&
                            errors.nascimento.type === 'required' && (
                                <span className={classes.span}>
                                    Este campo é obrigatório!
                                </span>
                            )}
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md>
                        <TextField
                            name="idade"
                            style={{ width: 100 }}
                            defaultValue={0}
                            label="Idade"
                            type="number"
                            InputLabelProps={{ shrink: true }}
                            variant="outlined"
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormSelect
                            name="sexo"
                            control={control}
                            label="Sexo"
                            defaultValue=""
                            required
                            fullWidth
                            onChange={handleSelectSex}
                            inputRef={register({ required: true })}
                            error={!!errors.sexo}
                        >
                            <MenuItem />
                            <MenuItem value={1}>Masculino</MenuItem>
                            <MenuItem value={2}>Feminino</MenuItem>
                            <MenuItem value={3}>Ignorado</MenuItem>
                        </FormSelect>
                        {errors.sexo && errors.sexo.type === 'required' && (
                            <span className={classes.span}>
                                Este campo é obrigatório!
                            </span>
                        )}
                    </Grid>
                    <Grid item md sm xs>
                        <FormSelect
                            name="raca_cor"
                            control={control}
                            label="Raça/Cor"
                            defaultValue=""
                            fullWidth
                            required
                            inputRef={register({ required: true })}
                            error={!!errors.raca}
                        >
                            <MenuItem />
                            <MenuItem value={1}>Branca</MenuItem>
                            <MenuItem value={2}>Preta</MenuItem>
                            <MenuItem value={3}>Amarela</MenuItem>
                            <MenuItem value={4}>Parda</MenuItem>
                            <MenuItem value={5}>Indígena</MenuItem>
                            <MenuItem value={6}>Sem Informação</MenuItem>
                        </FormSelect>
                        {errors.raca && errors.raca.type === 'required' && (
                            <span className={classes.span}>
                                Este campo é obrigatório!
                            </span>
                        )}
                    </Grid>
                    <Grid item md sm xs />
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <TextField
                            name="nome_mae"
                            label="Nome Completo da Mãe"
                            fullWidth
                            required
                            disabled={isDisabledMother}
                            inputRef={register({ required: !isDisabledMother })}
                            error={!!errors.nomeMae}
                        />
                        {errors.nomeMae &&
                            errors.nomeMae.type === 'required' && (
                                <span className={classes.span}>
                                    Este campo é obrigatório!
                                </span>
                            )}
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => handleDisabledMother()}
                                    name="mae_desconhecida"
                                    color="primary"
                                />
                            }
                            label="Desconhece o nome da mãe"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            name="nome_pai"
                            label="Nome Completo do Pai"
                            fullWidth
                            required
                            disabled={isDisabledDad}
                            inputRef={register({ required: !isDisabledDad })}
                            error={!!errors.nomePai}
                        />
                        {errors.nomePai &&
                            errors.nomePai.type === 'required' && (
                                <span className={classes.span}>
                                    Este campo é obrigatório!
                                </span>
                            )}
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => handleDisabledDad()}
                                    name="pai_desconhecido"
                                    color="primary"
                                />
                            }
                            label="Desconhece o nome do Pai"
                            variant="filled"
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <TextField
                            name="pisPasep"
                            label="Nº NIS (PIS/PASEP)"
                            type="number"
                            fullWidth
                            required
                            inputRef={register({ required: true })}
                            error={!!errors.pisPasep}
                        />
                        {errors.pisPasep &&
                            errors.pisPasep.type === 'required' && (
                                <span className={classes.span}>
                                    Este campo é obrigatório!
                                </span>
                            )}
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            name="tituloEleitor"
                            label="Título de Eleitor"
                            type="number"
                            fullWidth
                            required
                            inputRef={register({ required: true })}
                            error={!!errors.tituloEleitor}
                        />
                        {errors.tituloEleitor &&
                            errors.tituloEleitor.type === 'required' && (
                                <span className={classes.span}>
                                    Este campo é obrigatório!
                                </span>
                            )}
                    </Grid>
                    <Grid item md sm xs>
                        <FormSelect
                            name="nacionalidade"
                            label="Nacionalidade"
                            control={control}
                            defaultValue=""
                            fullWidth
                            select
                            required
                            onChange={handleNationality}
                            inputRef={register({ required: true })}
                            error={!!errors.nationality}
                        >
                            <MenuItem />
                            <MenuItem value={1}>Brasileira</MenuItem>
                            <MenuItem value={2}>Naturalizado</MenuItem>
                            <MenuItem value={3}>Estrangeiro</MenuItem>
                        </FormSelect>
                        {errors.nationality &&
                            errors.nationality.type === 'required' && (
                                <span className={classes.span}>
                                    Este campo é obrigatório!
                                </span>
                            )}
                    </Grid>
                    <Grid item md sm xs>
                        <Controller
                            render={({ onChange }) => (
                                <Autocomplete
                                    options={options}
                                    getOptionLabel={(option) => option.name}
                                    renderOption={(option) => option.name}
                                    disabled={nationality !== 3 && true}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            label="País de Nascimento"
                                            inputRef={register}
                                            InputProps={{
                                                ...params.InputProps,
                                                endAdornment: (
                                                    <>
                                                        {loading ? (
                                                            <CircularProgress
                                                                color="inherit"
                                                                size={20}
                                                            />
                                                        ) : null}
                                                        {
                                                            params.InputProps
                                                                .endAdornment
                                                        }
                                                    </>
                                                ),
                                            }}
                                        />
                                    )}
                                    onChange={(e, data) => onChange(data.name)}
                                    {...props}
                                />
                            )}
                            onChange={([, data]) => data}
                            defaultValue=""
                            name="pais_nascimento"
                            control={control}
                            rules={{ required: true }}
                            onFocus
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <Autocomplete
                            id="individuoVisitado"
                            fullWidth
                            required
                            disabled={nationality === 1 && true}
                            getOptionSelected={(option, value) =>
                                option.name === value.name
                            }
                            getOptionLabel={(option) => option.name}
                            options={options}
                            loading={loading}
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    label="Município de Nascimento"
                                    InputProps={{
                                        ...params.InputProps,
                                        endAdornment: (
                                            <>
                                                {loading ? (
                                                    <CircularProgress
                                                        color="inherit"
                                                        size={20}
                                                    />
                                                ) : null}
                                                {params.InputProps.endAdornment}
                                            </>
                                        ),
                                    }}
                                />
                            )}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            name="telefone_celular"
                            label="Telefone Celular"
                            type="text"
                            fullWidth
                            required
                            onChange={onchangePhone}
                            value={phone}
                            inputRef={register({ required: true })}
                            error={!!errors.telefoneCelular}
                        />
                        {errors.telefoneCelular &&
                            errors.telefoneCelular.type === 'required' && (
                                <span className={classes.span}>
                                    Este campo é obrigatório!
                                </span>
                            )}
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            name="email"
                            label="E-mail"
                            type="email"
                            fullWidth
                            inputRef={register}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <Typography variant="h6">
                            <strong>Informações Sociodemográficas</strong>
                        </Typography>
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormSelect
                            name="relacao_parentesco"
                            select
                            label="Relação de Parentesco com o Responsável Familiar"
                            control={control}
                            fullWidth
                            onChange={() => {}}
                            inputRef={register}
                        >
                            <MenuItem />
                            <MenuItem value={1}>
                                Conjuge / Companheiro(a)
                            </MenuItem>
                            <MenuItem value={2}>Filho(a)</MenuItem>
                            <MenuItem value={3}>Enteado(a)</MenuItem>
                            <MenuItem value={4}>Neto(a) / Bisneto(a)</MenuItem>
                            <MenuItem value={5}>Pai / Mãe</MenuItem>
                            <MenuItem value={6}>Irmão / Irmã</MenuItem>
                            <MenuItem value={7}>Genro / Nora</MenuItem>
                            <MenuItem value={8}>Outro parente</MenuItem>
                            <MenuItem value={9}>Não parente</MenuItem>
                        </FormSelect>
                    </Grid>
                    <Grid item md sm xs>
                        <Controller
                            render={({ onChange }) => (
                                <Autocomplete
                                    options={options}
                                    getOptionLabel={(option) => option.uuid}
                                    renderOption={(option) => option.uuid}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            label="Ocupação"
                                            type="number"
                                            inputRef={register}
                                            InputProps={{
                                                ...params.InputProps,
                                                endAdornment: (
                                                    <>
                                                        {loading ? (
                                                            <CircularProgress
                                                                color="inherit"
                                                                size={20}
                                                            />
                                                        ) : null}
                                                        {
                                                            params.InputProps
                                                                .endAdornment
                                                        }
                                                    </>
                                                ),
                                            }}
                                        />
                                    )}
                                    onChange={(e, data) => onChange(data.uuid)}
                                    {...props}
                                />
                            )}
                            onChange={([, data]) => data}
                            defaultValue=""
                            name="ocupacao_cod_cbo2002"
                            control={control}
                            rules={{ required: true }}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormSelect
                            name="situacaoConjugal"
                            control={control}
                            label="Situação Conjugal"
                            fullWidth
                            onChange={() => {}}
                            inputRef={register}
                        >
                            <MenuItem />
                            <MenuItem value={1}>Solteiro(a)</MenuItem>
                            <MenuItem value={2}>União Estável</MenuItem>
                            <MenuItem value={3}>Casado(a)</MenuItem>
                            <MenuItem value={4}>Separado(a)</MenuItem>
                            <MenuItem value={5}>Divorciado(a)</MenuItem>
                            <MenuItem value={6}>Viúvo(a)</MenuItem>
                            <MenuItem value={7}>outro</MenuItem>
                        </FormSelect>
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="funcionarioPublico"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Funcionário Público"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="beneficiarioBolsaFamilia"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Beneficiário do Bolsa Família"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="alfabetizado"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Sabe ler e escrever (é alfabetizado)"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="frequenta_escola"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Frequenta creche ou escola/estabelecimento de ensino"
                            variant="filled"
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormSelect
                            name="grau_instrucao"
                            control={control}
                            label="Qual é o curso mais elevado que frequenta ou frequentou"
                            required
                            inputRef={register({ required: true })}
                            fullWidth
                            error={!!errors.formacao}
                        >
                            <MenuItem />
                            <MenuItem value={1}>Creche</MenuItem>
                            <MenuItem value={2}>
                                Pré-escola (exceto CA)
                            </MenuItem>
                            <MenuItem value={3}>
                                Classe de alfabetização - CA
                            </MenuItem>
                            <MenuItem value={4}>
                                Ensino fundamental 1ª a 4ª séries
                            </MenuItem>
                            <MenuItem value={5}>
                                Ensino fundamental 5ª a 8ª séries
                            </MenuItem>
                            <MenuItem value={6}>
                                Ensino fundamental completo
                            </MenuItem>
                            <MenuItem value={7}>
                                Ensino fundamental especial
                            </MenuItem>
                            <MenuItem value={8}>
                                Ensino fundamental EJA - séries iniciais
                                (supletivo 1ª a 4ª)
                            </MenuItem>
                            <MenuItem value={9}>
                                Ensino fundamental EJA - séries finais
                                (supletivo 5ª a 8ª)
                            </MenuItem>
                            <MenuItem value={10}>
                                Ensino médio, médio 2º ciclo (científico,
                                técnico e etc
                            </MenuItem>
                            <MenuItem value={11}>
                                Ensino médio especial
                            </MenuItem>
                            <MenuItem value={12}>
                                Ensino Médio EJA (supletivo)
                            </MenuItem>
                            <MenuItem value={13}>
                                Superior, aperfeiçoamento,especialização,
                                mestrado,doutorado
                            </MenuItem>
                            <MenuItem value={14}>
                                Alfabetização para adultos (Mobral) etc
                            </MenuItem>
                            <MenuItem value={15}>Nenhum</MenuItem>
                        </FormSelect>
                        {errors.formacao &&
                            errors.formacao.type === 'required' && (
                                <span className={classes.span}>
                                    Este campo é obrigatório!
                                </span>
                            )}
                    </Grid>
                    <Grid item md sm xs>
                        <FormSelect
                            name="situacao_mercado_trabalho"
                            control={control}
                            label="Situação no Mercado de Trabalho"
                            inputRef={register}
                            fullWidth
                        >
                            <MenuItem />
                            <MenuItem value={1}>Empregador</MenuItem>
                            <MenuItem value={2}>
                                Assalariado com carteira de trabalho
                            </MenuItem>
                            <MenuItem value={3}>
                                Assalariado sem carteira de trabalho
                            </MenuItem>
                            <MenuItem value={4}>
                                Autônomo com previdência social
                            </MenuItem>
                            <MenuItem value={5}>
                                Autônomo sem previdência social
                            </MenuItem>
                            <MenuItem value={6}>
                                Aposentado / Pensionista
                            </MenuItem>
                            <MenuItem value={7}>Desempregado</MenuItem>
                            <MenuItem value={8}>Não trabalha</MenuItem>
                            <MenuItem value={9}>
                                Servidor público / Militar
                            </MenuItem>
                            <MenuItem value={10}>Outro</MenuItem>
                        </FormSelect>
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={disableSex}
                                    name="status_deseja_infromar_orientacao_sexual"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Deseja informar orientação sexual"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormSelect
                            name="orientacao_sexual"
                            control={control}
                            label="Orientação Sexual"
                            defaultValue=""
                            fullWidth
                            disabled={sex}
                            inputRef={register}
                        >
                            <MenuItem />
                            <MenuItem value={1}>Heterossexual</MenuItem>
                            <MenuItem value={2}>
                                Homossexual(gay / lésbica)
                            </MenuItem>
                            <MenuItem value={3}>Bissexual</MenuItem>
                            <MenuItem value={4}>outro</MenuItem>
                        </FormSelect>
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={disableGenre}
                                    name="status_deseja_informar_ident_genero"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Deseja informar identidade de gênero"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormSelect
                            name="indetidade_genero"
                            control={control}
                            label="Identidade de Gênero"
                            requireddefaultValue=""
                            fullWidth
                            disabled={genre}
                            inputRef={register}
                        >
                            <MenuItem />
                            <MenuItem value={1}>Homem transsexual</MenuItem>
                            <MenuItem value={2}>Mulher transsexual</MenuItem>
                            <MenuItem value={3}>Travesti</MenuItem>
                            <MenuItem value={4}>outro</MenuItem>
                        </FormSelect>
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    name="temcuidador"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Frequenta cuidador tradicional / curandeiros ou benzedeiras"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    name="grupoComunitario"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Participa de algum grupo comunitário"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    name="status_possui_plano_saude_privado"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Possui plano de saúde privado"
                            variant="filled"
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={handlePeopleCommunity}
                                    name="status_membro_povo_comunidade_tradicional"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Membro de povo ou comunidade tradicional? Se sim qual?"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            name="povoComunidade"
                            label="Povo ou Comunidade Tradicional"
                            fullWidth
                            disabled={peopleCommunity}
                            inputRef={register}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <Typography variant="h6">
                            <strong>Possui Alguma Deficiência?</strong>
                        </Typography>
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="status_tem_alguma_deficiencia"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Auditiva"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="status_tem_alguma_deficiencia"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Visual"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="status_tem_alguma_deficiencia"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Intelectual / Cognitiva"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="status_tem_alguma_deficiencia"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Física"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="status_tem_alguma_deficiencia"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Outra"
                            variant="filled"
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <Typography variant="h6">
                            <strong>Condições/Situações de Saúde Gerais</strong>
                        </Typography>
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={disabledMaternity}
                                    name="isgestante"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Está Gestante? Se SIM, qual é a meternidade de referência?"
                            variant="filled"
                            disabled={selectSex !== 2 && true}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            name="maternidade_referencia"
                            label="Maternidade de Referência"
                            fullWidth
                            disabled={maternity}
                            inputRef={register}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <TextField
                            name="peso"
                            label="Peso (Kg)"
                            type="number"
                            disabled={isDisabled}
                            fullWidth
                            onChange={(e) => setPeso(e.target.value)}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            name="altura"
                            label="Altura (cm)"
                            type="number"
                            disabled={isDisabled}
                            fullWidth
                            onChange={(e) => setAltura(e.target.value)}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            name="imc"
                            label="IMC (Índice de Massa Corporal)"
                            type="number"
                            fullWidth
                            disabled
                            value={Number(
                                (peso / (altura * altura)) * 10000
                            ).toFixed(1)}
                            onChange={() => {}}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormSelect
                            name="situacao_peso"
                            control={control}
                            label="Situação de Peso"
                            fullWidth
                            onChange={() => {}}
                            helperText="Porfavor selecione uma opção"
                            inputRef={register}
                        >
                            <MenuItem />
                            <MenuItem value={1}>Abaixo do peso</MenuItem>
                            <MenuItem value={2}>Peso adequado</MenuItem>
                            <MenuItem value={3}>Acima do peso</MenuItem>
                        </FormSelect>
                    </Grid>
                </Grid>
                {altura !== '' ? <ImcsCards /> : ''}
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="desnutrido"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Tem desnutrição (grave)?"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="status_fumante"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Está Fumante"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="status_dependente_alcool"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Faz Uso de Álcool"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="status_dependente_outras_drogas"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Faz Uso de Outras Drogas"
                            variant="filled"
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="status_teve_avc_derrame"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Teve AVC / Derrame"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="status_teve_infarto"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Teve Infarto"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="status_tem_tuberculose"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Tem Tuberculose"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="status_tem_hanseniase"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Está com Hanseníase"
                            variant="filled"
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="status_tem_diarreia"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Está com Diarréia"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="status_acamado"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Está Acamado"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="status_domiciliado"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Está Domiciliado"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="status_tem_hipertensao_arterial"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Tem Hipertensão Arterial"
                            variant="filled"
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={disabledHasCancer}
                                    name="status_tem_cancer"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Está com Câncer"
                            variant="filled"
                        />
                    </Grid>
                    {hasCancer ? (
                        <Grid item md sm xs>
                            <Autocomplete
                                name="tipo_tem_cancer"
                                fullWidth
                                required
                                open={open}
                                onOpen={() => {
                                    setOpen(true);
                                }}
                                onClose={() => {
                                    setOpen(false);
                                }}
                                getOptionSelected={(option, value) =>
                                    option.name === value.name
                                }
                                getOptionLabel={(option) => option.name}
                                options={options}
                                loading={loading}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        label="Está com qual tipo de câncer?"
                                        required
                                        InputProps={{
                                            ...params.InputProps,
                                            endAdornment: (
                                                <>
                                                    {loading ? (
                                                        <CircularProgress
                                                            color="inherit"
                                                            size={20}
                                                        />
                                                    ) : null}
                                                    {
                                                        params.InputProps
                                                            .endAdornment
                                                    }
                                                </>
                                            ),
                                        }}
                                    />
                                )}
                            />
                        </Grid>
                    ) : (
                        <Grid item md sm xs />
                    )}
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={disabledHadCancer}
                                    name="checked"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Já teve Câncer"
                            variant="filled"
                        />
                    </Grid>
                    {hadCancer ? (
                        <Grid item md sm xs>
                            <Autocomplete
                                name="tipo_tem_cancer"
                                fullWidth
                                required
                                open={open}
                                onOpen={() => {
                                    setOpen(true);
                                }}
                                onClose={() => {
                                    setOpen(false);
                                }}
                                getOptionSelected={(option, value) =>
                                    option.name === value.name
                                }
                                getOptionLabel={(option) => option.name}
                                options={options}
                                loading={loading}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        label="Teve qual tipo de câncer?"
                                        required
                                        InputProps={{
                                            ...params.InputProps,
                                            endAdornment: (
                                                <>
                                                    {loading ? (
                                                        <CircularProgress
                                                            color="inherit"
                                                            size={20}
                                                        />
                                                    ) : null}
                                                    {
                                                        params.InputProps
                                                            .endAdornment
                                                    }
                                                </>
                                            ),
                                        }}
                                    />
                                )}
                            />
                        </Grid>
                    ) : (
                        <Grid item md sm xs />
                    )}
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={disabledHasDiabetes}
                                    name="temDiabetes"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Tem Diabetes"
                            variant="filled"
                        />
                    </Grid>
                    {hasDiabetes ? (
                        <Grid item md sm xs>
                            <TextField
                                name="hasDiabetes"
                                select
                                label="Tipo de Diabetes"
                                required
                                onChange={() => {}}
                                helperText="Porfavor selecione uma opção"
                            >
                                <MenuItem />
                                <MenuItem value={1}>Tipo 1</MenuItem>
                                <MenuItem value={2}>Tipo 2</MenuItem>
                                <MenuItem value={3}>
                                    LADA (Latente Autoimune do Adulto
                                </MenuItem>
                                <MenuItem value={4}>Gestacional</MenuItem>
                            </TextField>
                        </Grid>
                    ) : (
                        <Grid item md sm xs />
                    )}
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="doido"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Fez ou faz tratamento com Psiquiatra ou teve internação por problemas de saúde mental"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="outrasPraticas"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Usa outras práicas integrativas ou complementares"
                            variant="filled"
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={disabledHasHospitalization}
                                    name="foiInternado"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Teve alguma Internação nosúltimos 12 meses? Se SIM, qual e por qual causa?"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            id="causaInternacao"
                            label="Causa da Internação"
                            fullWidth
                            disabled={hasHospitalization}
                            inputRef={register}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={disabledMedicinalPlants}
                                    name="usaPlantas"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Usa Plantas Medicinais? se SIM, quais Plantas"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            id="plantas"
                            label="Plantas Medicinais Usadas"
                            fullWidth
                            disabled={medicinalPlants}
                            inputRef={register}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="fazAtividadeFisica"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="É praticante de atividades físicas"
                            variant="filled"
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={hadheartdisease}
                                    name="foiCardiaca"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Teve doença cardiaca/do coração?"
                            variant="filled"
                        />
                    </Grid>
                    {haddisease ? (
                        <>
                            <Grid item md sm xs>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            onChange={() => {}}
                                            name="insuficienciaCardiaca"
                                            color="primary"
                                            inputRef={register}
                                        />
                                    }
                                    label="Insuficiência Cardiaca"
                                    variant="filled"
                                />
                            </Grid>
                            <Grid item md sm xs>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            onChange={() => {}}
                                            name="outra"
                                            color="primary"
                                            inputRef={register}
                                        />
                                    }
                                    label="Outra"
                                    variant="filled"
                                />
                            </Grid>
                            <Grid item md sm xs>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            onChange={() => {}}
                                            name="naoSabe"
                                            color="primary"
                                            inputRef={register}
                                        />
                                    }
                                    label="Não Sabe"
                                    variant="filled"
                                />
                            </Grid>
                        </>
                    ) : (
                        ''
                    )}
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={hashadKidneys}
                                    name="problemasRins"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Tem ou teve problemas nos rins?"
                            variant="filled"
                        />
                    </Grid>
                    {kidneys ? (
                        <>
                            <Grid item md sm xs>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            onChange={() => {}}
                                            name="insufRenal"
                                            color="primary"
                                            inputRef={register}
                                        />
                                    }
                                    label="Insuficiência Renal"
                                    variant="filled"
                                />
                            </Grid>
                            <Grid item md sm xs>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            onChange={() => {}}
                                            name="outraRenal"
                                            color="primary"
                                            inputRef={register}
                                        />
                                    }
                                    label="Outra"
                                    variant="filled"
                                />
                            </Grid>
                            <Grid item md sm xs>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            onChange={() => {}}
                                            name="naoSabeRenal"
                                            color="primary"
                                            inputRef={register}
                                        />
                                    }
                                    label="Não Sabe"
                                    variant="filled"
                                />
                            </Grid>
                        </>
                    ) : (
                        ''
                    )}
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={haveRespiratoryDisease}
                                    name="doencaPulmao"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Tem doença respiratória/no pulmão?"
                            variant="filled"
                        />
                    </Grid>
                    {repiratory ? (
                        <>
                            <Grid item md sm xs>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            onChange={() => {}}
                                            name="asma"
                                            color="primary"
                                            inputRef={register}
                                        />
                                    }
                                    label="Asma"
                                    variant="filled"
                                />
                            </Grid>
                            <Grid item md sm xs>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            onChange={() => {}}
                                            name="efisema"
                                            color="primary"
                                            inputRef={register}
                                        />
                                    }
                                    label="DPOC / Enfisema"
                                    variant="filled"
                                />
                            </Grid>
                            <Grid item md sm xs>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            onChange={() => {}}
                                            name="outraRespiratoria"
                                            color="primary"
                                            inputRef={register}
                                        />
                                    }
                                    label="Outra"
                                    variant="filled"
                                />
                            </Grid>
                            <Grid item md sm xs>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            onChange={() => {}}
                                            name="naoSabeRespiratoria"
                                            color="primary"
                                            inputRef={register}
                                        />
                                    }
                                    label="Não Sabe"
                                    variant="filled"
                                />
                            </Grid>
                        </>
                    ) : (
                        ''
                    )}
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <Typography variant="h6">
                            <strong>Outras Condições de Saúde</strong>
                        </Typography>
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <TextField
                            name="outrasCondicoes"
                            label="1 - Qual?"
                            fullWidth
                            inputRef={register}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <TextField
                            name="outrasCondicoes"
                            label="2 - Qual?"
                            fullWidth
                            inputRef={register}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <TextField
                            name="outrasCondicoes"
                            label="3 - Qual?"
                            fullWidth
                            inputRef={register}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={haveHomeless}
                                    name="situacaoRua"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Está em Situação de Rua"
                            variant="filled"
                        />
                    </Grid>
                    {homeless ? (
                        <Grid
                            container
                            spacing={4}
                            alignItems="center"
                            style={{ marginLeft: 1, marginRight: 5 }}
                        >
                            <Grid item md sm xs>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            onChange={() => {}}
                                            name="recebeBeneficio"
                                            color="primary"
                                            inputRef={register}
                                        />
                                    }
                                    label="Recebe Algum Benefício"
                                    variant="filled"
                                />
                            </Grid>
                            <Grid item md sm xs>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            onChange={() => {}}
                                            name="referenciaFamiliar"
                                            color="primary"
                                            inputRef={register}
                                        />
                                    }
                                    label="Possui Referência Familiar"
                                    variant="filled"
                                />
                            </Grid>
                            <Grid item md sm xs>
                                <FormSelect
                                    name="tempoRua"
                                    control={control}
                                    label="Tempo de situação de rua (período)"
                                    defaultValue=""
                                    fullWidth
                                    inputRef={register}
                                >
                                    <MenuItem />
                                    <MenuItem value={1}>
                                        Menos de 6 Meses
                                    </MenuItem>
                                    <MenuItem value={2}>6 a 12 Meses</MenuItem>
                                    <MenuItem value={3}>1 a 5 anos</MenuItem>
                                    <MenuItem value={4}>
                                        Mais de 5 anos
                                    </MenuItem>
                                </FormSelect>
                            </Grid>
                            <Grid item md sm xs>
                                <FormSelect
                                    name="vezesAlimenta"
                                    control={control}
                                    label="Quantas vezes se alimenta ao dia"
                                    defaultValue=""
                                    fullWidth
                                    inputRef={register}
                                >
                                    <MenuItem />
                                    <MenuItem value={1}>1 vez</MenuItem>
                                    <MenuItem value={2}>2 ou 3 vezes</MenuItem>
                                    <MenuItem value={3}>
                                        Mais de 3 vezes
                                    </MenuItem>
                                </FormSelect>
                            </Grid>
                            <Grid
                                container
                                spacing={4}
                                alignItems="center"
                                style={{ marginLeft: 2 }}
                            >
                                <Grid item md sm xs>
                                    <Typography variant="h6">
                                        <strong>
                                            Qual origem da alimentação?
                                        </strong>
                                    </Typography>
                                </Grid>
                            </Grid>
                            <Grid
                                container
                                spacing={4}
                                alignItems="center"
                                style={{ marginLeft: 1, marginRight: 5 }}
                            >
                                <Grid item md sm xs>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                onChange={() => {}}
                                                name="restPopular"
                                                color="primary"
                                                inputRef={register}
                                            />
                                        }
                                        label="Restaurante Popular"
                                        variant="filled"
                                    />
                                </Grid>
                                <Grid item md sm xs>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                onChange={() => {}}
                                                name="doacaoRestaurante"
                                                color="primary"
                                                inputRef={register}
                                            />
                                        }
                                        label="Doação Restaurante"
                                        variant="filled"
                                    />
                                </Grid>
                                <Grid item md sm xs>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                onChange={() => {}}
                                                name="grupoReligioso"
                                                color="primary"
                                                inputRef={register}
                                            />
                                        }
                                        label="Grupo Religioso"
                                        variant="filled"
                                    />
                                </Grid>
                                <Grid item md sm xs>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                onChange={() => {}}
                                                name="docaoPovo"
                                                color="primary"
                                                inputRef={register}
                                            />
                                        }
                                        label="Doação Popular"
                                        variant="filled"
                                    />
                                </Grid>
                                <Grid item md sm xs>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                onChange={() => {}}
                                                name="outros"
                                                color="primary"
                                                inputRef={register}
                                            />
                                        }
                                        label="Outros"
                                        variant="filled"
                                    />
                                </Grid>
                            </Grid>
                            <Grid
                                container
                                spacing={4}
                                alignItems="center"
                                style={{ marginLeft: 1, marginRight: 5 }}
                            >
                                <Grid item md sm xs>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                onChange={hasInstitution}
                                                name="acompanhado"
                                                color="primary"
                                                inputRef={register}
                                            />
                                        }
                                        label="É acompanhado por outra Instituição? se SIM, qual?"
                                        variant="filled"
                                    />
                                </Grid>
                                <Grid item md sm xs>
                                    <TextField
                                        name="instAcompanha"
                                        type="text"
                                        label="Outra instituição que acompanha"
                                        disabled={institution}
                                        inputRef={register}
                                    />
                                </Grid>
                            </Grid>
                            <Grid
                                container
                                spacing={4}
                                alignItems="center"
                                style={{ marginLeft: 2, marginRight: 5 }}
                            >
                                <Grid item md sm xs>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                onChange={hasKinship}
                                                name="visaitaFamilia"
                                                color="primary"
                                                inputRef={register}
                                            />
                                        }
                                        label="Visita algum familiar com frequência? Se SIM, qual grau de parentesco"
                                        variant="filled"
                                    />
                                </Grid>
                                <Grid item md sm xs>
                                    <TextField
                                        name="grauParentesco"
                                        type="text"
                                        label="Grau de parentesco"
                                        disabled={kinship}
                                    />
                                </Grid>
                            </Grid>
                            <Grid
                                container
                                spacing={4}
                                alignItems="center"
                                style={{ marginLeft: 2 }}
                            >
                                <Grid item md sm xs>
                                    <Typography variant="h6">
                                        <strong>
                                            Tem acesso a higiene pessoal?
                                        </strong>
                                    </Typography>
                                </Grid>
                            </Grid>
                            <Grid
                                container
                                spacing={4}
                                alignItems="center"
                                style={{ marginLeft: 1, marginRight: 5 }}
                            >
                                <Grid item md sm xs>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                onChange={() => {}}
                                                name="banho"
                                                color="primary"
                                                inputRef={register}
                                            />
                                        }
                                        label="Banho"
                                        variant="filled"
                                    />
                                </Grid>
                                <Grid item md sm xs>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                onChange={() => {}}
                                                name="acessoSanitario"
                                                color="primary"
                                                inputRef={register}
                                            />
                                        }
                                        label="Acesso ao sanitário"
                                        variant="filled"
                                    />
                                </Grid>
                                <Grid item md sm xs>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                onChange={() => {}}
                                                name="higieneBucal"
                                                color="primary"
                                                inputRef={register}
                                            />
                                        }
                                        label="Higiene bucal"
                                        variant="filled"
                                    />
                                </Grid>
                                <Grid item md sm xs>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                onChange={() => {}}
                                                name="outrosHigiene"
                                                color="primary"
                                                inputRef={register}
                                            />
                                        }
                                        label="Outros"
                                        variant="filled"
                                    />
                                </Grid>
                            </Grid>
                        </Grid>
                    ) : (
                        ''
                    )}
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="saidaCadastro"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Saída do cidadão do Cadastro"
                            variant="filled"
                            disabled
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="recusa_cadasro"
                                    color="primary"
                                    inputRef={register}
                                />
                            }
                            label="Recusa de cadastro"
                            variant="filled"
                        />
                    </Grid>
                    <Grid item md sm xs />
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <TextField
                            name="anotacoes"
                            label="Anotações"
                            type="textArea"
                            variant="outlined"
                            rows={4}
                            multiline
                            fullWidth
                            inputRef={register}
                        />
                    </Grid>
                </Grid>
            </div>
            <div className={classes.row}>
                <Button variant="contained" color="primary" type="submit">
                    Cadastrar
                </Button>

                <Button
                    variant="contained"
                    color="default"
                    onClick={props.close}
                >
                    Cancelar
                </Button>
            </div>
        </form>
    );
}
