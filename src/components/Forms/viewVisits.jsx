/* eslint-disable no-alert */
/* eslint-disable no-undef */
/* eslint-disable camelcase */
import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Grid,
    Typography,
    TextField,
    MenuItem,
    FormControlLabel,
    Checkbox,
    Button,
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { toast, ToastContainer } from 'react-toastify';
import { useForm, Controller } from 'react-hook-form';
import ImcsCards from '../Imcs';
import FormSelect from '../controlls/select/index';
import api from '../../services/api';

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '100%',
            height: '100%',
        },
    },
    container: {
        border: '1px solid #c1c1c1',
        borderRadius: '7px',
        margin: 'auto',
        marginTop: 10,
        justifyContent: 'space-between',
        padding: 20,
        overflowY: 'auto',
    },
    row: {
        display: 'flex',
        flexDirection: 'rows',
        justifyContent: 'space-between',
        margin: '2rem',
    },
    span: {
        display: 'flex',
        color: '#FF0000',
        fontSize: 11,
        marginLeft: 7,
        marginTop: -3,
    },
}));

export default function FormVisits(props) {
    const classes = useStyles();
    const { handleSubmit, errors, control, register } = useForm();
    const [peso, setPeso] = useState('');
    const [altura, setAltura] = useState('');
    const [isDisabled, setIsDisabled] = useState(false);
    const [options, setOptions] = useState([]);
    const [cidadaos, setCidadaos] = useState([]);
    const [selectedCidadao, setSelectedCidadao] = useState({
        nr_prontuario_ubs: '',
        cns_cidadao: '',
        data_nascimento: '',
    });
    const [families, setFamilies] = useState([]);
    const [familiesId, setFamiliesId] = useState('0');
    const [domiciliosId, setDomiciliosId] = useState(0);
    const [selectedDate, setSelectedDate] = React.useState(new Date());
    const user = JSON.parse(sessionStorage.getItem('USERAGENT'));

    useEffect(async () => {
        await api
            .get(`/domicilios?id=${user.id}`)
            .then((response) => {
                setOptions(response.data.domicilios);
            })
            .catch(Error);
    }, []);

    useEffect(async () => {
        const id = domiciliosId;
        if (id === '0') {
            return;
        }
        await api.get(`/familias?id=${id}`).then((response) => {
            setFamilies(response.data.familias);
        });
    }, [domiciliosId]);

    useEffect(async () => {
        const id = familiesId;
        if (id === '0') {
            return;
        }
        const response = await api.get(`/cidadaos?id=${id}`);
        setCidadaos(response.data.cidadaos);
    }, [familiesId]);

    const notifyError = (error) => {
        toast.error(
            `MMS - Error Notification -Operação não foi concluída!\n ${error}`,
            {
                position: toast.POSITION.TOP_RIGH,
            }
        );
    };

    const notifySuccess = () => {
        toast.success(
            'MMS - Success Notification - Operação efetuada com sucesso!',
            {
                position: toast.POSITION.TOP_RIGH,
            }
        );
    };

    const onSubmit = async (data) => {
        try {
            const response = await api.post('/visitacad', { user, data });
            notifySuccess(response);
            e.target.reset();
        } catch (error) {
            notifyError(error);
        }
    };

    const handleDateChange = (date) => {
        setSelectedDate(date);
    };

    const { nr_prontuario_ubs, cns_cidadao, data_nascimento } = selectedCidadao;

    return (
        <form
            className={classes.root}
            onSubmit={handleSubmit(onSubmit)}
            noValidate
            autoComplete="off"
        >
            <div className={classes.container}>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <Controller
                            render={({ onChange }) => (
                                <Autocomplete
                                    options={options}
                                    getOptionLabel={(option) =>
                                        `${option.nome} ${
                                            option.nome_logradouro
                                        }, ${option.numero || 'S/N'} ${
                                            option.complemento
                                        }`
                                    }
                                    renderOption={(option) =>
                                        `${option.nome} ${
                                            option.nome_logradouro
                                        }, ${option.numero || 'S/N'} ${
                                            option.complemento
                                        }`
                                    }
                                    getOptionSelected={(option) =>
                                        option.uuid_ficha_originadora
                                    }
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            name="cadastro_domicilio_uuid"
                                            label="Domicilio da Família"
                                            required
                                            error={
                                                !!errors.cadastro_domicilio_uuid
                                            }
                                        />
                                    )}
                                    onChange={(e, data) => {
                                        onChange(e.target.value);
                                        setDomiciliosId(
                                            data.uuid_ficha_originadora
                                        );
                                    }}
                                    {...props}
                                />
                            )}
                            onChange={([, data]) =>
                                setDomiciliosId(data.uuid_ficha_originadora)
                            }
                            name="cadastro_domicilio_uuid"
                            control={control}
                            rules={{ required: true }}
                            inputRef={register({
                                required: true,
                            })}
                            required
                            error={!!errors.cadastro_domicilio_uuid}
                        />
                        {errors.cadastro_domicilio_uuid &&
                            errors.cadastro_domicilio_uuid.type ===
                                'required' && (
                                <span className={classes.span}>
                                    Este campo é obrigatório!
                                </span>
                            )}
                    </Grid>
                    <Grid item md sm xs>
                        <Controller
                            render={({ onChange }) => (
                                <Autocomplete
                                    options={families}
                                    getOptionLabel={(option) =>
                                        `Prontuário nº ${option.prontuario_familiar}`
                                    }
                                    renderOption={(option) =>
                                        `Prontuário nº  ${option.prontuario_familiar}`
                                    }
                                    getOptionSelected={(option) =>
                                        setFamiliesId(option.id)
                                    }
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            label="Família"
                                            required
                                            inputRef={register({
                                                required: true,
                                            })}
                                            error={!!errors.familia_id}
                                        />
                                    )}
                                    onChange={(e) => {
                                        onChange(e.target.value);
                                    }}
                                    {...props}
                                />
                            )}
                            onChange={([, data]) => data.id}
                            defaultValue={options.id}
                            name="familia_id"
                            control={control}
                            rules={{ required: true }}
                            inputRef={register({
                                required: true,
                            })}
                            required
                            error={!!errors.familia_id}
                        />
                        {errors.familia_id &&
                            errors.familia_id.type === 'required' && (
                                <span className={classes.span}>
                                    Este campo é obrigatório!
                                </span>
                            )}
                    </Grid>
                    <Grid item md sm xs>
                        <Controller
                            render={({ onChange }) => (
                                <Autocomplete
                                    options={cidadaos}
                                    getOptionLabel={(option) =>
                                        option.nome_cidadao
                                    }
                                    renderOption={(option) =>
                                        option.nome_cidadao
                                    }
                                    getOptionSelected={(option) =>
                                        setSelectedCidadao(option)
                                    }
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            label="Indivíduo Visitado"
                                            required
                                            inputRef={register({
                                                required: true,
                                            })}
                                            error={
                                                !!errors.cadastro_cidadao_uuid
                                            }
                                        />
                                    )}
                                    onChange={(e) => {
                                        onChange(e.target.value);
                                    }}
                                    {...props}
                                />
                            )}
                            onChange={([, data]) => data.uuid}
                            defaultValue={options.uuid}
                            name="cadastro_cidadao_uuid"
                            control={control}
                            rules={{ required: true }}
                            inputRef={register({
                                required: true,
                            })}
                            required
                            error={!!errors.cadastro_cidadao_uuid}
                        />
                        {errors.cadastro_cidadao_uuid &&
                            errors.cadastro_cidadao_uuid.type ===
                                'required' && (
                                <span className={classes.span}>
                                    Este campo é obrigatório!
                                </span>
                            )}
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <TextField
                            name="prontuario_familiar"
                            fullWidth
                            disabled
                            label="Nº do Prontuário"
                            value={nr_prontuario_ubs || ''}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            name="cns_cidadao"
                            fullWidth
                            disabled
                            label="Nº do Cartão SUS"
                            value={cns_cidadao || ''}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            name="data_nascimento"
                            fullWidth
                            disabled
                            label="Data de Nascimento"
                            value={data_nascimento || ''}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <Typography variant="h6">
                            <strong>Informações da Visita</strong>
                        </Typography>
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <TextField
                            name="data_visita"
                            label="Data da Visita"
                            type="date"
                            defaultValue={selectedDate}
                            onChange={handleDateChange}
                            className={classes.textField}
                            InputLabelProps={{
                                shrink: true,
                            }}
                            inputRef={register({ required: true })}
                            error={!!errors.data_visita}
                        />
                        {errors.data_visita &&
                            errors.data_visita.type === 'required' && (
                                <span className={classes.span}>
                                    Este campo é obrigatório!
                                </span>
                            )}
                    </Grid>

                    <Grid item md sm xs>
                        <FormSelect
                            name="turno"
                            control={control}
                            label="Turno"
                            defaultValue=""
                            fullWidth
                            required
                            inputRef={register({ required: true })}
                            error={!!errors.turno}
                        >
                            <MenuItem />
                            <MenuItem value={1}>Manhã</MenuItem>
                            <MenuItem value={2}>Tarde</MenuItem>
                            <MenuItem value={3}>Noite</MenuItem>
                        </FormSelect>
                        {errors.turno && errors.turno.type === 'required' && (
                            <span className={classes.span}>
                                Este campo é obrigatório!
                            </span>
                        )}
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    inputRef={register}
                                    name="status_visita_compartilhada_outro_profissional"
                                    color="primary"
                                />
                            }
                            label="Visita compartilhada com outro profissional"
                            variant="filled"
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <Typography variant="h6">
                            <strong>Motivos da Visita</strong>
                        </Typography>
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <Typography variant="h6">
                            <strong>- Tipo da Visita</strong>
                        </Typography>
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    color="primary"
                                    disabled={isDisabled}
                                />
                            }
                            label="Cadastramento / Atualização"
                            variant="filled"
                            value={true && `1`}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    color="primary"
                                    disabled={isDisabled}
                                />
                            }
                            label="Visita periódica"
                            variant="filled"
                            value={true && `2`}
                            inputRef={register}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <Typography variant="h6">
                            <strong>- Busca Ativa</strong>
                        </Typography>
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Consulta"
                            variant="filled"
                            value={true && '3'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Exame"
                            variant="filled"
                            value={true && '4'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Vacina"
                            variant="filled"
                            value={true && '5'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Condicionalidades do bolsa família"
                            variant="filled"
                            value={true && '6'}
                            inputRef={register}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <Typography variant="h6">
                            <strong>- Acompanhamento</strong>
                        </Typography>
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Gestante"
                            variant="filled"
                            value={true && '7'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Puérpera"
                            variant="filled"
                            value={true && '8'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Recém-nascido"
                            variant="filled"
                            value={true && '9'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Criança"
                            variant="filled"
                            value={true && '10'}
                            inputRef={register}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Pessoa com desnutrição"
                            variant="filled"
                            value={true && '11'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Pessoa com reabilitação ou com deficiência"
                            variant="filled"
                            value={true && '12'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Pessoa com hipertensão"
                            variant="filled"
                            value={true && '13'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Pessoa com diabetes"
                            variant="filled"
                            value={true && '14'}
                            inputRef={register}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Pessoa com asma"
                            variant="filled"
                            value={true && '15'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Pessoa com DPOC / enfisema"
                            variant="filled"
                            value={true && '16'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Pessoa com câncer"
                            variant="filled"
                            value={true && '17'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Pessoa com outras doenças crônicas"
                            variant="filled"
                            value={true && '18'}
                            inputRef={register}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Pessoa com hanseníase"
                            variant="filled"
                            value={true && '19'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Pessoa com tuberculose"
                            variant="filled"
                            value={true && '20'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Sintomáticos respiratórios"
                            variant="filled"
                            value={true && '21'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Tabagista"
                            variant="filled"
                            value={true && '22'}
                            inputRef={register}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Domicíliados / Acamados"
                            variant="filled"
                            value={true && '23'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Condições de vulnerabilidade social"
                            variant="filled"
                            value={true && '24'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Condicionalidades do bolsa familia"
                            variant="filled"
                            value={true && '25'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Saúde mental"
                            variant="filled"
                            value={true && '26'}
                            inputRef={register}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Usuário de álcool"
                            variant="filled"
                            value={true && '27'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Usuário de outras drogas"
                            variant="filled"
                            value={true && '28'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs />
                    <Grid item md sm xs />
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <Typography variant="h6">
                            <strong>- Controle Ambiental / Vetorial</strong>
                        </Typography>
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Ação educativa"
                            variant="filled"
                            value={true && '29'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Imóvel com foco"
                            variant="filled"
                            value={true && '30'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Ação mecânica"
                            variant="filled"
                            value={true && '31'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Tratamento focal"
                            variant="filled"
                            value={true && '32'}
                            inputRef={register}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <Typography variant="h6">
                            <strong>- Outros</strong>
                        </Typography>
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Engresso de internação"
                            variant="filled"
                            value={true && '33'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Convite atividades coletivas / campanha de saúde"
                            variant="filled"
                            value={true && '34'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Orientação / Prevenção"
                            variant="filled"
                            value={true && '35'}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    onChange={() => {}}
                                    name="motivos_visita"
                                    disabled={isDisabled}
                                    color="primary"
                                />
                            }
                            label="Outros"
                            variant="filled"
                            value={true && '36'}
                            inputRef={register}
                        />
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <Typography variant="h6">
                            <strong>Antropometria</strong>
                        </Typography>
                    </Grid>
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <TextField
                            name="peso_acompanhamento_nutricional"
                            label="Peso (Kg)"
                            type="number"
                            disabled={isDisabled}
                            fullWidth
                            onChange={(e) => setPeso(e.target.value)}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            name="altura_acompanhamento_nutricional"
                            label="Altura (cm)"
                            type="number"
                            disabled={isDisabled}
                            fullWidth
                            onChange={(e) => setAltura(e.target.value)}
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            name="imc"
                            label="IMC (Índice de Massa Corporal)"
                            type="number"
                            fullWidth
                            disabled
                            value={Number(
                                (peso / (altura * altura)) * 10000
                            ).toFixed(1)}
                            onChange={() => {}}
                        />
                    </Grid>
                </Grid>
                {altura !== '' ? <ImcsCards /> : ''}
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <FormSelect
                            name="desfecho"
                            control={control}
                            select
                            fullWidth
                            label="Desfecho da Visita"
                            required
                            onChange={(e) => {
                                if (e.target.value !== 1) {
                                    setIsDisabled(true);
                                } else {
                                    setIsDisabled(false);
                                }
                            }}
                            inputRef={register({ required: true })}
                            error={!!errors.desfecho}
                        >
                            <MenuItem />
                            <MenuItem value={1}>Visita Realizada</MenuItem>
                            <MenuItem value={2}>Visita Recusada</MenuItem>
                            <MenuItem value={3}>Ausente</MenuItem>
                        </FormSelect>
                        {errors.desfecho &&
                            errors.desfecho.type === 'required' && (
                                <span className={classes.span}>
                                    Este campo é obrigatório!
                                </span>
                            )}
                    </Grid>
                    <Grid item md sm xs>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    inputRef={register}
                                    name="necessita_visita_endemias"
                                    color="primary"
                                />
                            }
                            label="Necessita da visita de um agente de endemias"
                            variant="filled"
                            inputRef={register}
                        />
                    </Grid>
                    <Grid item md sm xs />
                </Grid>
                <Grid container spacing={4} alignItems="center">
                    <Grid item md sm xs>
                        <TextField
                            name="anotacao"
                            label="Anotações"
                            multiline
                            rows={4}
                            variant="outlined"
                            inputRef={register}
                        />
                    </Grid>
                </Grid>
            </div>
            <div className={classes.row}>
                <Button variant="contained" color="primary" type="submit">
                    Cadastrar
                </Button>

                <Button
                    variant="contained"
                    color="default"
                    onClick={props.close}
                >
                    Cancelar
                </Button>
            </div>
            <ToastContainer />
        </form>
    );
}
