/* eslint-disable no-undef */
/* eslint-disable no-alert */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-use-before-define */
import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import {
    Grid,
    Button,
    FormControlLabel,
    Switch,
    Typography,
    TextField,
    makeStyles,
} from '@material-ui/core';
import { useForm } from 'react-hook-form';
import { Face, Fingerprint } from '@material-ui/icons';
import { toast, ToastContainer } from 'react-toastify';
import api from '../../services/api';
import { login, getLogin, removeLogin } from '../../services/auth';

const useStyles = makeStyles(() => ({
    span: {
        display: 'flex',
        color: '#FF0000',
        fontSize: 11,
        marginLeft: 8,
        marginTop: 1,
    },
}));

export default function FormSignIn() {
    const history = useHistory();
    const classes = useStyles();

    const { handleSubmit, errors, register } = useForm();
    const [userName, setUserName] = useState('');
    const [rememberMe, setRememberMe] = useState(false);

    useEffect(() => {
        getRememberedUser();
    }, [userName]);

    const onSubmit = async (getData) => {
        const { username, password, lembrarme } = getData;

        try {
            const response = await api.post('/authenticate', {
                username,
                password,
            });
            login(response.data.token, response.data.user);
            history.push('/loginAgent');
            if (lembrarme === true) {
                getLogin(username);
            } else {
                removeLogin();
            }
        } catch (err) {
            notify();
        }
    };

    const notify = () => {
        toast.error('MMS - Error Notification - Usuário ou Senha inválidos!', {
            position: toast.POSITION.TOP_RIGH,
        });
    };
    const handleChange = () => {
        setRememberMe((prev) => !prev);
    };

    const getRememberedUser = async () => {
        const username = await JSON.parse(localStorage.getItem('USERNAME'));
        if (username === null || username === undefined) {
            setRememberMe(false);
        } else {
            setUserName(username);
            setRememberMe(true);
        }
    };

    return (
        <form
            className={classes.root}
            onSubmit={handleSubmit(onSubmit)}
            noValidate
            autoComplete="on"
        >
            <Grid container spacing={4} alignItems="center">
                <Grid item>
                    <Face />
                </Grid>
                <Grid item md sm xs>
                    <TextField
                        name="username"
                        label="Usuário"
                        type="text"
                        inputRef={register({ required: true })}
                        fullWidth0
                        defaultValue={JSON.parse(
                            localStorage.getItem('USERNAME')
                        )}
                        required
                        error={!!errors.username}
                    />
                    {errors.username && errors.username.type === 'required' && (
                        <span className={classes.span}>
                            Este campo é obrigatório!
                        </span>
                    )}
                </Grid>
            </Grid>
            <Grid container spacing={4} alignItems="center">
                <Grid item>
                    <Fingerprint />
                </Grid>
                <Grid item md sm xs>
                    <TextField
                        name="password"
                        label="Senha"
                        type="password"
                        inputRef={register({ required: true })}
                        fullWidth
                        required
                        error={!!errors.password}
                    />
                    {errors.password && errors.password.type === 'required' && (
                        <span className={classes.span}>
                            Este campo é obrigatório!
                        </span>
                    )}
                </Grid>
            </Grid>
            <Grid
                container
                spacing={4}
                justify="space-between"
                alignItems="center"
            >
                <Grid item />
                <Grid item />
                <Grid item>
                    <Typography>Lembrar-me</Typography>
                </Grid>
                <Grid item>
                    <FormControlLabel
                        control={
                            <Switch
                                color="primary"
                                checked={rememberMe}
                                onChange={handleChange}
                                name="lembrarme"
                                inputRef={register}
                            />
                        }
                    />
                </Grid>
            </Grid>

            <Grid
                container
                justify="center"
                style={{
                    marginTop: '10px',
                    marginBottom: '20px',
                    maxWidth: 450,
                }}
            >
                <Button
                    variant="contained"
                    color="primary"
                    type="submit"
                    fullWidth
                >
                    Login
                </Button>
            </Grid>
            <ToastContainer />
        </form>
    );
}
