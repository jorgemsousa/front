/* eslint-disable camelcase */
import React, { useEffect, useState } from 'react';
import { TextField, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import api from '../../services/api';

const useStyles = makeStyles(() => ({
    root: {
        display: 'flex',
        margin: '1rem',
        flexDirection: 'column',
    },
    input: {
        background: '#F5F5F5',
    },

    container: {
        margin: '0 0 1rem 0',
    },
}));

export default function Perfil() {
    const classes = useStyles();
    const User = JSON.parse(sessionStorage.getItem('USER'));
    const [loged, setLoged] = useState({});

    useEffect(async () => {
        api.get(`/users/user/${User.id}`)
            .then((response) => setLoged(response.data))
            .catch(Error);
    }, []);

    const {
        nome,
        email,
        login,
        matricula,
        cep,
        logradouro,
        numero,
        bairro,
        complemento,
        cidade,
        estado,
        telefone,
        dta_nascimento,
        nr_cnes,
        data_hora_incl,
        situacao,
    } = loged;
    console.log(nome);
    return (
        <>
            <form className={classes.root}>
                <Grid
                    container
                    spacing={4}
                    alignItems="center"
                    className={classes.container}
                >
                    <Grid item md sm xs>
                        <TextField
                            className={classes.input}
                            name="nome"
                            label="Nome Completo"
                            disabled
                            value={nome}
                            fullWidth
                            type="text"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            variant="outlined"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            className={classes.input}
                            name="email"
                            label="E-mail"
                            disabled
                            value={email}
                            fullWidth
                            type="text"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            variant="outlined"
                        />
                    </Grid>
                </Grid>
                <Grid
                    container
                    spacing={4}
                    alignItems="center"
                    className={classes.container}
                >
                    <Grid item md sm xs>
                        <TextField
                            className={classes.input}
                            name="login"
                            label="Login"
                            disabled
                            value={login}
                            fullWidth
                            type="text"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            variant="outlined"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            className={classes.input}
                            name="matricula"
                            label="Matricula"
                            disabled
                            value={matricula}
                            fullWidth
                            type="text"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            variant="outlined"
                        />
                    </Grid>
                </Grid>
                <Grid
                    container
                    spacing={4}
                    alignItems="center"
                    className={classes.container}
                >
                    <Grid item md sm xs>
                        <TextField
                            className={classes.input}
                            name="cep"
                            label="CEP"
                            disabled
                            value={cep}
                            fullWidth
                            type="text"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            variant="outlined"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            className={classes.input}
                            name="logradouro"
                            label="Logradouro"
                            disabled
                            value={logradouro}
                            fullWidth
                            type="text"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            variant="outlined"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            className={classes.input}
                            name="numero"
                            label="Número"
                            disabled
                            value={numero}
                            fullWidth
                            type="text"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            variant="outlined"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            className={classes.input}
                            name="bairro"
                            label="Bairro"
                            disabled
                            value={bairro}
                            fullWidth
                            type="text"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            variant="outlined"
                        />
                    </Grid>
                </Grid>
                <Grid
                    container
                    spacing={4}
                    alignItems="center"
                    className={classes.container}
                >
                    <Grid item md sm xs>
                        <TextField
                            className={classes.input}
                            name="complemento"
                            label="Complemento"
                            disabled
                            value={complemento}
                            fullWidth
                            type="text"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            variant="outlined"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            className={classes.input}
                            name="cidade"
                            label="Cidade"
                            disabled
                            defaultValue={cidade}
                            fullWidth
                            type="text"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            variant="outlined"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            className={classes.input}
                            name="estado"
                            label="Estado"
                            disabled
                            defaultValue={estado}
                            fullWidth
                            type="text"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            variant="outlined"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            className={classes.input}
                            name="telefone"
                            label="Telefone"
                            disabled
                            defaultValue={telefone}
                            fullWidth
                            type="text"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            variant="outlined"
                        />
                    </Grid>
                </Grid>
                <Grid
                    container
                    spacing={4}
                    alignItems="center"
                    className={classes.container}
                >
                    <Grid item md sm xs>
                        <TextField
                            className={classes.input}
                            name="dta_nascimento"
                            label="Data de Nascimento"
                            disabled
                            defaultValue={dta_nascimento}
                            fullWidth
                            type="text"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            variant="outlined"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            className={classes.input}
                            name="nr_cns"
                            label="Número do CNS"
                            disabled
                            defaultValue={nr_cnes}
                            fullWidth
                            type="text"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            variant="outlined"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            className={classes.input}
                            name="data_hora_incl"
                            label="Data de Cadastro"
                            disabled
                            defaultValue={data_hora_incl}
                            fullWidth
                            type="text"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            variant="outlined"
                        />
                    </Grid>
                    <Grid item md sm xs>
                        <TextField
                            className={classes.input}
                            name="situacao"
                            label="Situação"
                            disabled
                            defaultValue={situacao}
                            fullWidth
                            type="text"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            variant="outlined"
                        />
                    </Grid>
                </Grid>
            </form>
        </>
    );
}
