import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import CardIMC from '../Cards/cardIMC';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
        },
    },
}));

export default function ImcsCards() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container spacing={4}>
                <Grid item md sm xs>
                    <CardIMC
                        title="Abaixo de 15"
                        bcolor="#FF0000"
                        text="Extremamente abaixo do peso"
                        bcontentcolor="#F5A9A9"
                    />
                </Grid>
                <Grid item md sm xs>
                    <CardIMC
                        title="Entre 15 e 16"
                        bcolor="#FFA500"
                        text="Gravemente abaixo do peso"
                        bcontentcolor="#F5DA81"
                    />
                </Grid>
                <Grid item md sm xs>
                    <CardIMC
                        title="Entre 16 e 18.5"
                        bcolor="#FFA500"
                        text="Abaixo do peso ideal"
                        bcontentcolor="#F5DA81"
                    />
                </Grid>
                <Grid item md sm xs>
                    <CardIMC
                        title="Entre 18.5 e 25"
                        bcolor="#00FF00"
                        text="Peso Ideal"
                        bcontentcolor="#9FF781"
                    />
                </Grid>
            </Grid>
            <Grid container spacing={4}>
                <Grid item md sm xs>
                    <CardIMC
                        title="Entre 25 e 30"
                        bcolor="#FFFF00"
                        text="Sobrepeso"
                        bcontentcolor="#F3F781"
                    />
                </Grid>
                <Grid item md sm xs>
                    <CardIMC
                        title="Entre 30 e 35"
                        bcolor="#FFA500"
                        text="Obesidade grau I"
                        bcontentcolor="#F5DA81"
                    />
                </Grid>
                <Grid item md sm xs>
                    <CardIMC
                        title="Entre 35 e 40"
                        bcolor="#FF0000"
                        text="Obesidade grau II (grave)"
                        bcontentcolor="#F5A9A9"
                    />
                </Grid>
                <Grid item md sm xs>
                    <CardIMC
                        title="Acima de 40"
                        bcolor="#FF0000"
                        text="Obesidade grau III (mórbida)"
                        bcontentcolor="#F5A9A9"
                    />
                </Grid>
            </Grid>
        </div>
    );
}
