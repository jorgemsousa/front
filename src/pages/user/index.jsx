import React from 'react';
import { Paper, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Perfil from '../../components/Forms/perfil';

const useStyles = makeStyles(() => ({
    root: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        margin: '2rem 5rem',
        padding: '4rem',
        backgroundColor: '#FFF',
        borderRadius: 5,
        flexDirection: 'column',
    },
}));

export default function User() {
    const classes = useStyles();

    return (
        <>
            <Paper className={classes.root}>
                <Typography>
                    <h3>Perfil do Usuário</h3>
                </Typography>
                <Perfil />
            </Paper>
        </>
    );
}
