import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Modal,
    Backdrop,
    Fade,
    Button,
    Typography,
    Icon,
} from '@material-ui/core';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import CancelIcon from '@material-ui/icons/Cancel';
import Table from '../../../components/Tables/individualsReg';
import FormIndividual from '../../../components/Forms/regIndividuals';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    head: {
        margin: 30,
    },
    table: {
        marginLeft: 40,
        marginRight: 40,
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        width: '95%',
        height: '95%',
        overflowY: 'scroll',
    },
    row: {
        display: 'flex',
        flexDirection: 'rows',
        justifyContent: 'space-between',
    },
}));

export default function Homes() {
    const classes = useStyles();
    const [open, setOpen] = useState(false);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const [collum] = React.useState([
        {
            id: 'nomeCidadao',
            label: 'Nome do Cidadão',
            minWidth: 170,
            align: 'left',
        },
        {
            id: 'nomeMaeCidadao',
            label: 'Nome da Mãe',
            minWidth: 170,
            align: 'left',
        },
        {
            id: 'nomePaiCidadao',
            label: 'Nome do Pai',
            minWidth: 170,
            align: 'left',
        },
        {
            id: 'dataNascimentoCidadao',
            label: 'Data de Nascimento',
            minWidth: 170,
            align: 'left',
        },
        {
            id: 'microarea',
            label: 'Micro-Área',
            minWidth: 170,
            align: 'left',
        },
        {
            id: 'acoes',
            label: 'Ações',
            minWidth: 100,
            align: 'center',
        },
    ]);

    return (
        <>
            <div className={classes.head}>
                <Button>Lista de Indivíduos</Button>

                <Button
                    style={{ float: 'right' }}
                    variant="contained"
                    color="primary"
                    size="medium"
                    startIcon={<AddCircleIcon />}
                    onClick={handleOpen}
                >
                    Novo Cadastro
                </Button>

                <Button style={{ float: 'right', marginRight: 30 }}>
                    Exportar
                </Button>
            </div>

            <Table className={classes.table} arrayCollum={collum} />
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={classes.paper}>
                        <Typography component="h3" className={classes.row}>
                            <strong>Cadastro de Indivíduos</strong>
                            <Icon color="primary">
                                <CancelIcon
                                    onClick={handleClose}
                                    cursor="pointer"
                                />
                            </Icon>
                        </Typography>
                        <FormIndividual close={handleClose} />
                    </div>
                </Fade>
            </Modal>
        </>
    );
}
