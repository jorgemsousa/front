import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Button,
    Modal,
    Backdrop,
    Fade,
    Typography,
    Icon,
} from '@material-ui/core';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import CancelIcon from '@material-ui/icons/Cancel';
import Table from '../../../components/Tables/familiesReg';
import FormFamily from '../../../components/Forms/regFamilies';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    head: {
        margin: 30,
    },
    table: {
        marginLeft: 40,
        marginRight: 40,
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        width: '95%',
        height: '95%',
        overflowY: 'scroll',
    },
    row: {
        display: 'flex',
        flexDirection: 'rows',
        justifyContent: 'space-between',
    },
}));

export default function Homes() {
    const classes = useStyles();

    const [collum] = React.useState([
        {
            id: 'prontuarioFamiliar',
            label: 'Prontuário',
            minWidth: 170,
            align: 'left',
        },
        {
            id: 'resideDesde',
            label: 'Reside Desde',
            minWidth: 170,
            align: 'left',
        },
        {
            id: 'unidade',
            label: 'Unidade de Atendimento',
            minWidth: 170,
            align: 'left',
        },
        {
            id: 'acoes',
            label: 'Ações',
            minWidth: 100,
            align: 'center',
        },
    ]);

    const [open, setOpen] = useState(false);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <>
            <div className={classes.head}>
                <Button>Lista de Famílias</Button>

                <Button
                    style={{ float: 'right' }}
                    variant="contained"
                    color="primary"
                    size="medium"
                    startIcon={<AddCircleIcon />}
                    onClick={handleOpen}
                >
                    Novo Cadastro
                </Button>

                <Button style={{ float: 'right', marginRight: 30 }}>
                    Exportar
                </Button>
            </div>

            <Table className={classes.table} arrayCollum={collum} />
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={classes.paper}>
                        <Typography component="h3" className={classes.row}>
                            <strong>Cadastro Famíliar</strong>
                            <Icon color="primary">
                                <CancelIcon
                                    onClick={handleClose}
                                    cursor="pointer"
                                />
                            </Icon>
                        </Typography>
                        <FormFamily close={handleClose} />
                    </div>
                </Fade>
            </Modal>
        </>
    );
}
