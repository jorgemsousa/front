/* eslint-disable no-undef */
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import {
    AppBar,
    Toolbar,
    Grid,
    Typography,
    Avatar,
    Divider,
    Menu,
    MenuItem,
} from '@material-ui/core';
import CssBaseline from '@material-ui/core/CssBaseline';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Sidebar from '../../components/Sidebar/index';
import Logo from '../../assets/images/logo.svg';
import { logout } from '../../services/auth';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        backgroundColor: '#EEEEEE',
        paddingBottom: 30,
        marginBottom: 20,
        height: '100vh',
    },
    nested: {
        paddingLeft: theme.spacing(4),
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9) + 1,
        },
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    logo: {
        width: 280,
        height: 140,
        marginTop: -15,
        marginBottom: -60,
    },
    span: {
        fontSize: 14,
        marginRight: 10,
    },
    user: {
        display: 'inline',
        padding: '9px 13px',
        marginRight: 10,
        backgroundColor: '#EE1289',
        border: '1px solid #fff',
    },
    menu: {
        textDecoration: 'none',
        color: 'inherit',
    },
}));

export default function Partial() {
    const classes = useStyles();
    const theme = useTheme();
    const [open, setOpen] = useState(false);
    const [anchorEl, setAnchorEl] = React.useState(null);
    const openMenu = Boolean(anchorEl);
    const User = JSON.parse(sessionStorage.getItem('USER'));
    const UserAgent = JSON.parse(sessionStorage.getItem('USERAGENT'));

    const options = [
        <Link to="/loginAgent" className={classes.menu}>
            Alterar Agente
        </Link>,
        <Link to="/perfil" className={classes.menu}>
            Perfil
        </Link>,
        <Link
            to="*"
            onClick={(e) => {
                e.preventDefault();
                logout();
            }}
            className={classes.menu}
        >
            Sair
        </Link>,
    ];

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <>
            <div className={classes.root}>
                <CssBaseline />
                <AppBar
                    position="fixed"
                    className={clsx(classes.appBar, {
                        [classes.appBarShift]: open,
                    })}
                >
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            onClick={handleDrawerOpen}
                            edge="start"
                            className={clsx(classes.menuButton, {
                                [classes.hide]: open,
                            })}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Grid
                            container
                            direction="row"
                            justify="space-between"
                            alignItems="center"
                        >
                            <Typography variant="h6" noWrap>
                                MMS - SISPEC
                            </Typography>

                            {UserAgent ? (
                                <Typography>
                                    Agente: {UserAgent.nome}
                                </Typography>
                            ) : (
                                ''
                            )}

                            <div>
                                <IconButton
                                    aria-label="more"
                                    color="inherit"
                                    aria-controls="long-menu"
                                    aria-haspopup="true"
                                    onClick={handleClick}
                                >
                                    <MoreVertIcon />
                                </IconButton>
                                <Menu
                                    id="long-menu"
                                    anchorEl={anchorEl}
                                    keepMounted
                                    open={openMenu}
                                    onClose={handleClose}
                                    PaperProps={{
                                        style: {
                                            marginTop: '3ch',
                                            marginLeft: '2ch',
                                            width: '20ch',
                                        },
                                    }}
                                >
                                    {options.map((option) => (
                                        <MenuItem
                                            key={option}
                                            selected={option === 'Pyxis'}
                                            onClick={handleClose}
                                        >
                                            {option}
                                        </MenuItem>
                                    ))}
                                </Menu>
                                <Avatar
                                    alt={User.nome}
                                    src={
                                        User.foto != null
                                            ? User.foto
                                            : User.nome.substring(0, 1)
                                    }
                                    className={classes.user}
                                />
                                <span className={classes.span}>
                                    {User.nome}
                                </span>
                                <IconButton color="inherit" onClick={logout}>
                                    <ExitToAppIcon />
                                </IconButton>
                            </div>
                        </Grid>
                    </Toolbar>
                </AppBar>
                <Drawer
                    variant="permanent"
                    className={clsx(classes.drawer, {
                        [classes.drawerOpen]: open,
                        [classes.drawerClose]: !open,
                    })}
                    classes={{
                        paper: clsx({
                            [classes.drawerOpen]: open,
                            [classes.drawerClose]: !open,
                        }),
                    }}
                >
                    <div className={classes.toolbar}>
                        <img src={Logo} alt="Logo" className={classes.logo} />
                        <IconButton onClick={handleDrawerClose}>
                            {theme.direction === 'rtl' ? (
                                <ChevronRightIcon />
                            ) : (
                                <>
                                    <ChevronLeftIcon />
                                </>
                            )}
                        </IconButton>
                    </div>
                    <Divider />
                    <Sidebar />
                </Drawer>
            </div>
        </>
    );
}
