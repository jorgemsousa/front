import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';

import Error from '../../assets/images/error.svg';

const useStyles = makeStyles(() => ({
    root: {
        marginLeft: '17%',
    },
    text: {},
}));

export default function SimpleContainer() {
    const classes = useStyles();

    return (
        <>
            <CssBaseline />
            <Container maxWidth="sm" className={classes.root}>
                <img src={Error} alt="Error" />
                <strong className={classes.text}>Página não encontrada</strong>
            </Container>
        </>
    );
}
