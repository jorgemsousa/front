import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Table from '../../components/Tables/homesInc';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    head: {
        margin: 30,
    },
    table: {
        marginLeft: 40,
        marginRight: 40,
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        width: '90%',
        height: '80%',
    },
}));

export default function Homes() {
    const classes = useStyles();

    const [collum] = React.useState([
        {
            id: 'logradouro',
            label: 'Nome Logradouro',
            minWidth: 170,
            align: 'left',
        },
        {
            id: 'numero',
            label: 'Número',
            minWidth: 170,
            align: 'left',
        },
        {
            id: 'bairro',
            label: 'Bairro',
            minWidth: 170,
            align: 'left',
        },
        {
            id: 'microarea',
            label: 'Micro-Área',
            minWidth: 170,
            align: 'left',
        },
        {
            id: 'acoes',
            label: 'Ações',
            minWidth: 100,
            align: 'center',
        },
    ]);

    return (
        <>
            <div className={classes.head}>
                <Button>Inconsistências de Domicílios</Button>

                <Button style={{ float: 'right', marginRight: 30 }}>
                    Exportar
                </Button>
            </div>

            <Table className={classes.table} arrayCollum={collum} />
        </>
    );
}
