import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Table from '../../components/Tables/individualsInc';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    head: {
        margin: 30,
    },
    table: {
        marginLeft: 40,
        marginRight: 40,
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        width: '90%',
        height: '80%',
    },
}));

export default function Homes() {
    const classes = useStyles();

    const [collum] = React.useState([
        {
            id: 'nome',
            label: 'Nome Indivíduo',
            minWidth: 170,
            align: 'left',
        },
        {
            id: 'mae',
            label: 'Nome da Mãe',
            minWidth: 170,
            align: 'left',
        },
        {
            id: 'pai',
            label: 'Nome do Pai',
            minWidth: 170,
            align: 'left',
        },
        {
            id: 'nascimento',
            label: 'Data de Nascimento',
            minWidth: 170,
            align: 'left',
        },
        {
            id: 'microarea',
            label: 'Micro-Área',
            minWidth: 170,
            align: 'left',
        },
        {
            id: 'acoes',
            label: 'Ações',
            minWidth: 100,
            align: 'center',
        },
    ]);

    return (
        <>
            <div className={classes.head}>
                <Button>Inconsistências de Indivíduos</Button>

                <Button style={{ float: 'right', marginRight: 30 }}>
                    Exportar
                </Button>
            </div>

            <Table className={classes.table} arrayCollum={collum} />
        </>
    );
}
