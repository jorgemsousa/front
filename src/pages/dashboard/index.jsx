/* eslint-disable no-undef */
import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import HomeIcon from '@material-ui/icons/Home';
import PersonIcon from '@material-ui/icons/Person';
import ViewListIcon from '@material-ui/icons/ViewList';
import { ToastContainer } from 'react-toastify';
import CardFigure from '../../components/Cards/caardFigure';
import Card from '../../components/Cards/cardData';

import api from '../../services/api';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    row: {
        flexDirection: 'row',
    },
}));

export default function CenteredGrid() {
    const classes = useStyles();

    const [countCitizens, setCountCitizens] = useState([]);
    const [countResidences, setCountResidences] = useState([]);

    const [countResidencesReg, setCountResidencesReg] = useState([]);
    const [countCitizensReg, setCountCitizensReg] = useState([]);
    const [countFamilyReg, setCountFamilyReg] = useState([]);
    const [countVisitReg, setCountVisitReg] = useState([]);
    const { id } = JSON.parse(sessionStorage.getItem('USERAGENT')) || '';

    useEffect(() => {
        api.get('cidadao')
            .then((response) => {
                setCountCitizens(response.data.size);
            })
            .catch((error) => error);
    }, []);

    useEffect(() => {
        api.get('domicilio')
            .then((response) => {
                setCountResidences(response.data.size);
            })
            .catch((error) => error);
    }, []);

    useEffect(() => {
        api.get(`domiciliocad?id=${id}`)
            .then((response) => {
                setCountResidencesReg(response.data.size);
            })
            .catch((error) => error);
    }, [id]);

    useEffect(() => {
        api.get(`cidadaocad?id=${id}`)
            .then((response) => {
                setCountCitizensReg(response.data.size);
            })
            .catch((error) => error);
    }, [id]);

    useEffect(() => {
        api.get(`familiacad?id=${id}`)
            .then((response) => {
                setCountFamilyReg(response.data.size);
            })
            .catch((error) => error);
    }, [id]);

    useEffect(() => {
        api.get(`visitacad?id=${id}`)
            .then((response) => {
                setCountVisitReg(response.data.size);
            })
            .catch((error) => error);
    }, [id]);

    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs>
                    <Card
                        elevation={6}
                        text="Domicílios"
                        icon={<HomeIcon />}
                        color="#0000FF"
                        link="/ihomes"
                        type="Inconsistências"
                        data={countResidences}
                    />
                </Grid>
                <Grid item xs>
                    <Card
                        elevation={6}
                        text="Indivíduos"
                        icon={<PersonIcon />}
                        color="#006400"
                        link="/iindividuals"
                        type="Inconsistências"
                        data={countCitizens}
                    />
                </Grid>
                <Grid item xs>
                    <Card
                        elevation={6}
                        text="Total"
                        icon={<ViewListIcon />}
                        color="#FF0000"
                        type="Inconsistências"
                        data={countResidences + countCitizens}
                    />
                </Grid>
            </Grid>
            {id !== '' ? (
                <Grid container spacing={3}>
                    <Grid item xs={3}>
                        <CardFigure
                            text="Domicílios Ativos do Agente"
                            data={countResidencesReg}
                            link="/rhomes"
                        />
                    </Grid>
                    <Grid item xs={3}>
                        <CardFigure
                            text="Indivíduos Ativos do Agente"
                            data={countCitizensReg}
                            link="/rindividuals"
                        />
                    </Grid>
                    <Grid item xs={3}>
                        <CardFigure
                            text="Familias Cadastradas do Agente"
                            data={countFamilyReg}
                            link="/rfamilies"
                        />
                    </Grid>
                    <Grid item xs={3}>
                        <CardFigure
                            text="Visitas Efetuadas do Agente"
                            data={countVisitReg}
                            link="/rvisits"
                        />
                    </Grid>
                </Grid>
            ) : (
                <Grid container spacing={3} />
            )}
            <Grid container spacing={3} />
            <ToastContainer />
        </div>
    );
}
