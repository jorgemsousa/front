import React from 'react';
import { Paper, Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Logo from '../../assets/images/logo.svg';

import FormLogin from '../../components/Forms/login';

const useStyles = makeStyles(() => ({
    root: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        margin: '5rem 20rem',
        padding: '3rem',
        backgroundColor: '#FFF',
        borderRadius: 5,
        flexDirection: 'column',
    },
    text: {
        textAlign: 'center',
        marginBottom: '1rem',
        marginTop: '-20rem',
    },
    img: {
        marginTop: '-5rem',
        maxWidth: '30rem',
    },
    form: {
        width: 600,
        margin: '1rem',
    },
    foot: {
        marginTop: '2rem',
    },
}));

export default function Login() {
    const classes = useStyles();
    const city = 'Laranjeiras';

    return (
        <>
            <Paper className={classes.root}>
                <Grid
                    container
                    direction="column"
                    justify="center"
                    alignItems="center"
                >
                    <Grid item md sm xs>
                        <img src={Logo} alt="Logo" className={classes.img} />
                    </Grid>
                    <Grid item className={classes.text}>
                        <Typography variant="h3">SISPEC</Typography>
                        <Typography variant="h5">
                            Prontuário Eletrônico do Cidadão
                        </Typography>
                        <Typography variant="h4">{city} / SE</Typography>
                    </Grid>
                    <FormLogin className={classes.form} />
                </Grid>
            </Paper>
        </>
    );
}
