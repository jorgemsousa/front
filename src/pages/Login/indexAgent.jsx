/* eslint-disable no-unused-expressions */
/* eslint-disable no-use-before-define */
import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Modal, Fade, Icon, Typography, Backdrop } from '@material-ui/core';
import CancelIcon from '@material-ui/icons/Cancel';
import 'react-toastify/dist/ReactToastify.css';
import FormAgent from '../../components/Forms/formAgent';
import { logout } from '../../services/auth';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    head: {
        margin: 30,
    },
    table: {
        marginLeft: 40,
        marginRight: 40,
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        width: '65%',
        height: '75%',
        overflowY: 'scroll',
        borderRadius: 8,
    },
    row: {
        display: 'flex',
        flexDirection: 'rows',
        justifyContent: 'space-between',
        fontSize: '1.5rem',
    },
}));

export default function FormPropsTextFields() {
    const classes = useStyles();
    const [open, setOpen] = useState(false);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    useEffect(() => {
        handleOpen();
    }, []);

    return (
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            className={classes.modal}
            open={open}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 5000,
            }}
        >
            <Fade in={open}>
                <div className={classes.paper}>
                    <Typography component="h3" className={classes.row}>
                        <strong>Selecione os dados do Agente de Saúde!</strong>
                        <Icon color="primary">
                            <CancelIcon
                                onClick={(handleClose, logout)}
                                cursor="pointer"
                            />
                        </Icon>
                    </Typography>
                    <FormAgent close={handleClose} />
                </div>
            </Fade>
        </Modal>
    );
}
